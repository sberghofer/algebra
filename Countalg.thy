theory Countalg
imports Polynomial "~~/src/HOL/Number_Theory/Residues"
begin

definition Decode :: "('a, 'm) partial_object_scheme \<Rightarrow> nat \<Rightarrow> 'a" ("decode\<index> _" [81] 80)
where "Decode R = (SOME f. range f = carrier R)"

definition Encode :: "('a, 'm) partial_object_scheme \<Rightarrow> 'a \<Rightarrow> nat" ("encode\<index> _" [81] 80)
where "Encode R = Hilbert_Choice.inv (Decode R)"

locale countable_ring = ring +
  assumes ex_decode: "\<exists>f::nat\<Rightarrow>'a. range f = carrier R"
begin

lemma encode_inverse: "x \<in> carrier R \<Longrightarrow> decode (encode x) = x"
  apply (simp add: Encode_def Decode_def)
  apply (rule f_inv_into_f)
  apply (simp add: someI_ex [OF ex_decode])
  done

lemma  decode_carrier: "decode y \<in> carrier R"
  by (simp add: Decode_def)
    (rule rangeI [of "SOME f::nat\<Rightarrow>'a. range f = carrier R", simplified someI_ex [OF ex_decode]])

end

locale countable_field = field + countable_ring

primrec
  diag :: "nat \<Rightarrow> (nat \<times> nat)"
where
  "diag 0 = (0, 0)"
| "diag (Suc n) =
     (let (x, y) = diag n
      in case y of
          0 \<Rightarrow> (0, Suc x)
        | Suc y \<Rightarrow> (Suc x, y))"

theorem diag_le1: "fst (diag (Suc n)) < Suc n"
  by (induct n) (simp_all add: Let_def split_def split: nat.split)

theorem diag_le2: "snd (diag (Suc (Suc n))) < Suc (Suc n)"
  apply (induct n)
  apply (simp_all add: Let_def split_def split: nat.split nat.split_asm)
  apply (rule impI)
  apply (case_tac n)
  apply simp
  apply hypsubst
  apply (rule diag_le1)
  done

theorem diag_le3: "fst (diag n) = Suc x \<Longrightarrow> snd (diag n) < n"
  apply (case_tac n)
  apply simp
  apply (case_tac nat)
  apply (simp add: Let_def)
  apply hypsubst
  apply (rule diag_le2)
  done

theorem diag_le4: "fst (diag n) = Suc x \<Longrightarrow> x < n"
  apply (case_tac n)
  apply simp
  apply (case_tac nat)
  apply (simp add: Let_def)
  apply hypsubst_thin
  apply (drule sym)
  apply (drule ord_eq_less_trans)
  apply (rule diag_le1)
  apply simp
  done

function
  undiag :: "nat \<times> nat \<Rightarrow> nat"
where
  "undiag (0, 0) = 0"
| "undiag (0, Suc y) = Suc (undiag (y, 0))"
| "undiag (Suc x, y) = Suc (undiag (x, Suc y))"
  by pat_completeness auto

termination
  by (relation "measure (\<lambda>(x, y). ((x + y) * (x + y + 1)) div 2 + x)") auto

theorem diag_undiag [simp]: "diag (undiag (x, y)) = (x, y)"
  by (rule undiag.induct) (simp add: Let_def)+

definition int_to_nat :: "int \<Rightarrow> nat"
where "int_to_nat i = undiag (nat i, nat (- i))"

definition nat_to_int :: "nat \<Rightarrow> int"
where "nat_to_int n = int (fst (diag n)) - int (snd (diag n))"

lemma int_to_nat_inverse: "nat_to_int (int_to_nat i) = i"
  by (simp add: int_to_nat_def nat_to_int_def)

definition nat_to_int' :: "int ring \<Rightarrow> nat \<Rightarrow> int"
where "nat_to_int' R n = (if nat_to_int n \<in> carrier R then nat_to_int n else \<zero>\<^bsub>R\<^esub>)"

primrec encode_list :: "('a \<Rightarrow> nat) \<Rightarrow> 'a list \<Rightarrow> nat"
where
  "encode_list e [] = 0"
| "encode_list e (x # xs) = undiag (Suc (e x), encode_list e xs)"

function decode_list :: "(nat \<Rightarrow> 'a) \<Rightarrow> nat \<Rightarrow> 'a list"
where
  "decode_list d n = (case fst (diag n) of
       0 \<Rightarrow> []
     | Suc m \<Rightarrow> d m # decode_list d (snd (diag n)))"
  by pat_completeness auto

termination
  by (relation "measure snd") (auto intro: diag_le3 diag_le4)

lemma encode_list_inverse:
  assumes inv: "\<forall>x\<in>set xs. d (e x) = x"
  shows "decode_list d (encode_list e xs) = xs"
  using inv
  by (induct xs) simp_all

sublocale residues < countable_ring
  apply unfold_locales
  apply (rule exI [of _ "nat_to_int' R"])
  apply (auto simp add: nat_to_int'_def)
  apply (rule_tac x="int_to_nat x" in range_eqI)
  apply (simp add: nat_to_int'_def int_to_nat_inverse)
  done

sublocale residues_prime < countable_field
  by unfold_locales

primrec ext_enum_poly :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> nat \<Rightarrow> 'a list"
where
  "ext_enum_poly R p 0 = p"
| "ext_enum_poly R p (Suc i) =
     (let d = fst (pgcd\<^bsub>R\<^esub> (ext_enum_poly R p i) (pnormalize\<^bsub>R\<^esub> (decode_list (Decode R) i)))
      in if length d > 1 then d else ext_enum_poly R p i)"

lemma (in countable_ring) decode_list_closed [simp]:
  "in_carrier (decode_list (Decode R) i)"
  by (induct "Decode R" i rule: decode_list.induct) (simp add: decode_carrier split: nat.split)

lemma (in countable_field) ext_enum_poly_closed [simp]:
  "in_carrier p \<Longrightarrow> pnormal p \<Longrightarrow> in_carrier (ext_enum_poly R p i)"
  "in_carrier p \<Longrightarrow> pnormal p \<Longrightarrow> pnormal (ext_enum_poly R p i)"
  by (induct i) (simp_all add: Let_def del: decode_list.simps)

lemma (in countable_field) dv_ext_enum_poly:
  assumes "i \<le> j" "in_carrier p" "pnormal p"
  shows "ext_enum_poly R p j pdvd ext_enum_poly R p i"
  using assms
  apply (induct j arbitrary: i)
  apply (auto simp add: le_Suc_eq Let_def simp del: decode_list.simps)
  apply (rule pdvd_trans)
  apply (simp del: decode_list.simps)
  apply (rule pgcd_pdvd(1))
  apply (simp_all del: decode_list.simps)
  done

lemma ext_enum_poly_length: "1 < length p \<Longrightarrow> 1 < length (ext_enum_poly R p i)"
  by (induct i) (simp_all add: Let_def)

definition ext_ideal :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> 'a list \<Rightarrow> bool"
where "ext_ideal R p q = ext_enum_poly R p (Suc (encode_list (Encode R) q)) pdvd\<^bsub>R\<^esub> q"

lemma (in countable_ring) encode_inverse_in_carrier:
  "in_carrier xs \<Longrightarrow> \<forall>x\<in>set xs. decode (encode x) = x"
  by (induct xs) (simp_all add: encode_inverse)

lemma (in countable_field) ext_ideal_coprime:
  "in_carrier p \<Longrightarrow> in_carrier q \<Longrightarrow> pnormal p \<Longrightarrow> pnormal q \<Longrightarrow> p \<noteq> [] \<Longrightarrow>
   \<not> ext_ideal R p q \<Longrightarrow> i > encode_list (Encode R) q \<Longrightarrow> coprimep q (ext_enum_poly R p i)"
  apply (simp add: ext_ideal_def del: ext_enum_poly.simps)
  apply (rule coprime_pdvd [OF _ _ _ _ _ _ dv_ext_enum_poly [of "Suc (encode_list (Encode R) q)"]])
  apply (simp_all del: ext_enum_poly.simps)
  apply (rule ccontr)
  apply (erule notE [of "ext_enum_poly R p (Suc (encode_list (Encode R) q)) pdvd q"])
  apply (simp add: encode_inverse_in_carrier encode_list_inverse Let_def del: decode_list.simps)
  apply (rule conjI)
  apply (rule impI)
  apply (rule pgcd_pdvd)
  apply simp
  apply assumption
  apply simp
  apply assumption
  apply (rule impI)
  apply simp
  apply (cases "fst (pgcd q (ext_enum_poly R p (encode_list (Encode R) q)))")
  apply (simp add: pgcd_nil_iff)
  apply (insert pgcd_monic [of q "ext_enum_poly R p (encode_list (Encode R) q)"])
  apply (simp add: pgcd_commute)
  done

lemma (in countable_field) mem_ext_ideal:
  "1 < length p \<Longrightarrow> in_carrier p \<Longrightarrow> in_carrier q \<Longrightarrow> pnormal p \<Longrightarrow> pnormal q \<Longrightarrow>
   ext_ideal R p q = (\<exists>i. ext_enum_poly R p i pdvd\<^bsub>R\<^esub> q)"
  apply (subgoal_tac "p \<noteq> []")
  prefer 2
  apply auto[1]
  apply (auto simp add: ext_ideal_def simp del: ext_enum_poly.simps del: pdividesE)
  apply (cut_tac x=i and y="encode_list (Encode R) q" in le_less_linear)
  apply (erule disjE)
  apply (subst ext_enum_poly.simps(2))
  apply (simp add: encode_inverse_in_carrier encode_list_inverse Let_def del: decode_list.simps)
  apply (intro conjI impI)
  apply (rule pgcd_pdvd)
  apply auto[4]
  apply (rule pdvd_trans)
  apply simp
  apply (rule dv_ext_enum_poly)
  apply assumption+
  apply (fold ext_ideal_def)
  apply (cut_tac p=p and q=q and i=i in ext_ideal_coprime [simplified atomize_imp])
  apply simp
  apply (drule not_mono)
  apply (erule impE)
  apply (frule_tac i=i and R=R in ext_enum_poly_length [simplified])
  apply (cut_tac x=q and y="ext_enum_poly R p i" in pgcd_absorb2)
  apply simp_all
  apply auto[1]
  apply (rule notI)
  apply (drule_tac f=length and y="[\<one>]" in arg_cong)
  apply (subgoal_tac "ext_enum_poly R p i \<noteq> []")
  apply (simp add: poly_cmult_length_eq nonzero_imp_inverse_nonzero)
  apply auto
  done

definition ext_equiv :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> ('a list \<times> 'a list) set"
where "ext_equiv R p = {(q, q').
  ext_ideal R p (q ---\<^bsub>R\<^esub> q') \<and>
  in_carrier\<^bsub>R\<^esub> q \<and> pnormal\<^bsub>R\<^esub> q \<and> in_carrier\<^bsub>R\<^esub> q' \<and> pnormal\<^bsub>R\<^esub> q'}"

definition repr_of :: "'a set \<Rightarrow> 'a"
where "repr_of P = (SOME x. x \<in> P)"

lemma repr_of_proj_equiv:
  "equiv S R \<Longrightarrow> x \<in> S \<Longrightarrow> (x, repr_of (Equiv_Relations.proj R x)) \<in> R"
  apply (simp add: repr_of_def proj_def)
  apply (rule someI)
  apply (erule equivE)
  apply (erule refl_onD)
  apply assumption
  done

lemma proj_repr_of_eq: "equiv S R \<Longrightarrow> x \<in> S // R \<Longrightarrow> Equiv_Relations.proj R (repr_of x) = x"
  by (simp add: repr_of_def proj_Eps)

lemma repr_of_quot: "equiv S R \<Longrightarrow> x \<in> S // R \<Longrightarrow> repr_of x \<in> S"
  by (simp add: repr_of_def equiv_Eps_preserves)

lemma equiv_trans:
  "equiv S R \<Longrightarrow> (x, y) \<in> R \<Longrightarrow> (y, z) \<in> R \<Longrightarrow> (x, z) \<in> R"
  by (auto elim: equivE transE)

lemma equiv_sym:
  "equiv S R \<Longrightarrow> (x, y) \<in> R \<Longrightarrow> (y, x) \<in> R"
  by (auto elim: equivE symE)

lemma equiv_refl:
  "equiv S R \<Longrightarrow> x \<in> S \<Longrightarrow> (x, x) \<in> R"
  by (auto elim: equivE refl_onD)

definition poly_set :: "('a, 'm) ring_scheme \<Rightarrow> 'a list set" where
  "poly_set R = {p. in_carrier\<^bsub>R\<^esub> p \<and> pnormal\<^bsub>R\<^esub> p}"

lemma poly_setI [intro!]: "in_carrier\<^bsub>R\<^esub> p \<Longrightarrow> pnormal\<^bsub>R\<^esub> p \<Longrightarrow> p \<in> poly_set R"
  by (simp add: poly_set_def)

lemma poly_setE [elim!]: "p \<in> poly_set R \<Longrightarrow> (in_carrier\<^bsub>R\<^esub> p \<Longrightarrow> pnormal\<^bsub>R\<^esub> p \<Longrightarrow> P) \<Longrightarrow> P"
  by (simp add: poly_set_def)

definition mk_ext_field :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> 'a list set ring"
where "mk_ext_field R p =
  \<lparr>carrier=poly_set R//ext_equiv R p,
   monoid.mult=(\<lambda>a b. Equiv_Relations.proj (ext_equiv R p) (repr_of a ***\<^bsub>R\<^esub> repr_of b)),
   one=Equiv_Relations.proj (ext_equiv R p) [\<one>\<^bsub>R\<^esub>],
   zero=Equiv_Relations.proj (ext_equiv R p) [],
   add=(\<lambda>a b. Equiv_Relations.proj (ext_equiv R p) (repr_of a +++\<^bsub>R\<^esub> repr_of b))\<rparr>"

definition inv_ext_field :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> 'a list set \<Rightarrow> 'a list set"
where "inv_ext_field R p q = Equiv_Relations.proj (ext_equiv R p)
  (fst (snd (pgcd\<^bsub>R\<^esub> (repr_of q) (ext_enum_poly R p (Suc (encode_list (Encode R) (repr_of q)))))))"

locale countable_field_ext = countable_field +
  fixes p
  assumes carrier_p: "in_carrier p"
  and normal_p: "pnormal p"
  and length_p: "1 < length p"

lemma (in countable_field_ext) equiv_ext_equiv:
  "equiv (poly_set R) (ext_equiv R p)"
proof (rule equivI)
  show "refl_on (poly_set R) (ext_equiv R p)"
    by (rule refl_onI) (auto simp add: ext_equiv_def poly_set_def ext_ideal_def)
next
  show "sym (ext_equiv R p)"
  proof (rule symI)
    fix a b
    assume "(a, b) \<in> ext_equiv R p"
    with length_p carrier_p normal_p
    obtain i q where "in_carrier a" "in_carrier b" "pnormal a" "pnormal b"
      "in_carrier q" "ext_enum_poly R p i *** q = a --- b"
      by (auto simp add: ext_equiv_def mem_ext_ideal)
    with `in_carrier p` `pnormal p` have "b --- a = ext_enum_poly R p i *** -- q" by simp
    then have "ext_enum_poly R p i pdvd (b --- a)"
      by (rule pdividesI) (simp add: `in_carrier q`)
    with `1 < length p` `in_carrier p` `pnormal p`
      `in_carrier a` `in_carrier b` `pnormal a` `pnormal b`
    show "(b, a) \<in> ext_equiv R p"
      by (auto simp add: ext_equiv_def mem_ext_ideal)
  qed
next
  show "trans (ext_equiv R p)"
  proof (rule transI)
    fix a b c
    assume "(a, b) \<in> ext_equiv R p" "(b, c) \<in> ext_equiv R p"
    with length_p carrier_p normal_p
    obtain i j where "in_carrier a" "in_carrier b" "in_carrier c"
      "pnormal a" "pnormal b" "pnormal c"
      "ext_enum_poly R p i pdvd (a --- b)" "ext_enum_poly R p j pdvd (b --- c)"
      by (auto simp add: ext_equiv_def mem_ext_ideal del: pdividesE)
    from `in_carrier p` `pnormal p`
    have "ext_enum_poly R p (max i j) pdvd ext_enum_poly R p i"
      by (simp_all add: dv_ext_enum_poly)
    with _ have "ext_enum_poly R p (max i j) pdvd (a --- b)"
    using `ext_enum_poly R p i pdvd (a --- b)`
      by (rule pdvd_trans) (simp add: `in_carrier p` `pnormal p`)
    then obtain q where "ext_enum_poly R p (max i j) *** q = a --- b" "in_carrier q"
      by auto
    moreover from `in_carrier p` `pnormal p`
    have "ext_enum_poly R p (max i j) pdvd ext_enum_poly R p j"
      by (simp_all add: dv_ext_enum_poly)
    with _ have "ext_enum_poly R p (max i j) pdvd (b --- c)"
    using `ext_enum_poly R p j pdvd (b --- c)`
      by (rule pdvd_trans) (simp add: `in_carrier p` `pnormal p`)
    then obtain q' where "ext_enum_poly R p (max i j) *** q' = b --- c" "in_carrier q'"
      by auto
    ultimately have "a --- c = ext_enum_poly R p (max i j) *** (q +++ q')"
      using `in_carrier p` `pnormal p` `in_carrier a` `in_carrier b` `in_carrier c`
      by (simp add: pmult_r_distr padd_pminus_eq pminus_padd_eq)
        (simp add: padd_pminus_eq [symmetric])
    then have "ext_enum_poly R p (max i j) pdvd (a --- c)"
      by (rule pdividesI) (simp add: `in_carrier q` `in_carrier q'`)
    with `1 < length p` `in_carrier p` `pnormal p`
      `in_carrier a` `in_carrier c` `pnormal a` `pnormal c`
    show "(a, c) \<in> ext_equiv R p"
      by (auto simp add: ext_equiv_def mem_ext_ideal)
  qed
qed

lemma equiv_proj':
  "equiv A r \<Longrightarrow> (x, y) \<in> r \<Longrightarrow> Equiv_Relations.proj r x = Equiv_Relations.proj r y"
  by (auto simp add: Equiv_Relations.proj_def intro!: equiv_class_eq)

lemma eq_equiv_class':
  "equiv A r \<Longrightarrow> y \<in> A \<Longrightarrow> Equiv_Relations.proj r x = Equiv_Relations.proj r y \<Longrightarrow> (x, y) \<in> r"
  by (auto simp add: Equiv_Relations.proj_def intro: eq_equiv_class)

lemma (in countable_field_ext) in_carrier_repr_quot:
  "x \<in> poly_set R // ext_equiv R p \<Longrightarrow> in_carrier (repr_of x)"
  by (auto dest!: repr_of_quot [OF equiv_ext_equiv])

lemma (in countable_field_ext) normal_repr_quot:
  "x \<in> poly_set R // ext_equiv R p \<Longrightarrow> pnormal (repr_of x)"
  by (auto dest!: repr_of_quot [OF equiv_ext_equiv])

lemma (in countable_field_ext) addp_compat:
  assumes "(a, a') \<in> ext_equiv R p" "(b, b') \<in> ext_equiv R p"
  shows "(a +++ b, a' +++ b') \<in> ext_equiv R p"
proof -
    from `(a, a') \<in> ext_equiv R p` `(b, b') \<in> ext_equiv R p`
      length_p carrier_p normal_p
    obtain i j where "in_carrier a" "in_carrier a'" "in_carrier b" "in_carrier b'"
      "pnormal a" "pnormal a'" "pnormal b" "pnormal b'"
      "ext_enum_poly R p i pdvd (a --- a')" "ext_enum_poly R p j pdvd (b --- b')"
      by (auto simp add: ext_equiv_def mem_ext_ideal del: pdividesE)
    from `in_carrier p` `pnormal p`
    have "ext_enum_poly R p (max i j) pdvd ext_enum_poly R p i"
      by (simp_all add: dv_ext_enum_poly)
    with _ have "ext_enum_poly R p (max i j) pdvd (a --- a')"
    using `ext_enum_poly R p i pdvd (a --- a')`
      by (rule pdvd_trans) (simp add: `in_carrier p` `pnormal p`)
    then obtain q where "ext_enum_poly R p (max i j) *** q = a --- a'" "in_carrier q"
      by auto
    moreover from `in_carrier p` `pnormal p`
    have "ext_enum_poly R p (max i j) pdvd ext_enum_poly R p j"
      by (simp_all add: dv_ext_enum_poly)
    with _ have "ext_enum_poly R p (max i j) pdvd (b --- b')"
    using `ext_enum_poly R p j pdvd (b --- b')`
      by (rule pdvd_trans) (simp add: `in_carrier p` `pnormal p`)
    then obtain q' where "ext_enum_poly R p (max i j) *** q' = b --- b'" "in_carrier q'"
      by auto
    ultimately have "(a +++ b) --- (a' +++ b') = ext_enum_poly R p (max i j) *** (q +++ q')"
      using `in_carrier p` `pnormal p` `in_carrier a` `in_carrier a'` `in_carrier b` `in_carrier b'`
      by (simp add: pmult_r_distr padd_pminus_eq pminus_padd_eq pminus_pminus_eq)
    then have "ext_enum_poly R p (max i j) pdvd ((a +++ b) --- (a' +++ b'))"
      by (rule pdividesI) (simp add: `in_carrier q` `in_carrier q'`)
    with `1 < length p` `in_carrier p` `pnormal p`
      `in_carrier a` `in_carrier a'` `in_carrier b` `in_carrier b'`
    show ?thesis
      by (auto simp add: ext_equiv_def mem_ext_ideal)
qed

lemma (in countable_field_ext) pmult_compat:
  assumes "(a, a') \<in> ext_equiv R p" "(b, b') \<in> ext_equiv R p"
  shows "(a *** b, a' *** b') \<in> ext_equiv R p"
proof -
    from `(a, a') \<in> ext_equiv R p` `(b, b') \<in> ext_equiv R p`
      length_p carrier_p normal_p
    obtain i j where "in_carrier a" "in_carrier a'" "in_carrier b" "in_carrier b'"
      "pnormal a" "pnormal a'" "pnormal b" "pnormal b'"
      "ext_enum_poly R p i pdvd (a --- a')" "ext_enum_poly R p j pdvd (b --- b')"
      by (auto simp add: ext_equiv_def mem_ext_ideal del: pdividesE)
    from `in_carrier p` `pnormal p`
    have "ext_enum_poly R p (max i j) pdvd ext_enum_poly R p i"
      by (simp_all add: dv_ext_enum_poly)
    with _ have "ext_enum_poly R p (max i j) pdvd (a --- a')"
    using `ext_enum_poly R p i pdvd (a --- a')`
      by (rule pdvd_trans) (simp add: `in_carrier p` `pnormal p`)
    then obtain q where "ext_enum_poly R p (max i j) *** q = a --- a'" "in_carrier q"
      by auto
    moreover from `in_carrier p` `pnormal p`
    have "ext_enum_poly R p (max i j) pdvd ext_enum_poly R p j"
      by (simp_all add: dv_ext_enum_poly)
    with _ have "ext_enum_poly R p (max i j) pdvd (b --- b')"
    using `ext_enum_poly R p j pdvd (b --- b')`
      by (rule pdvd_trans) (simp add: `in_carrier p` `pnormal p`)
    then obtain q' where "ext_enum_poly R p (max i j) *** q' = b --- b'" "in_carrier q'"
      by auto
    ultimately have "(a *** b) --- (a' *** b') = ext_enum_poly R p (max i j) *** (q *** b +++ q' *** a')"
      using `in_carrier p` `pnormal p` `in_carrier a` `in_carrier a'` `in_carrier b` `in_carrier b'`
      by (simp add: pmult_r_distr padd_pminus_eq pmult_assoc [symmetric] pmult_pminus_l_distr)
        (simp add: pmult_comm)
    then have "ext_enum_poly R p (max i j) pdvd ((a *** b) --- (a' *** b'))"
      by (rule pdividesI) (simp add: `in_carrier q` `in_carrier q'` `in_carrier a'` `in_carrier b`)
    with `1 < length p` `in_carrier p` `pnormal p`
      `in_carrier a` `in_carrier a'` `in_carrier b` `in_carrier b'`
    show ?thesis
      by (auto simp add: ext_equiv_def mem_ext_ideal)
qed

sublocale countable_field_ext < cf: countable_field "mk_ext_field R p"
  apply (rule countable_field.intro)
  apply (rule cring.cring_fieldI)
proof (unfold_locales, goal_cases)
  case (1 x y) \<comment> \<open> addition is closed \<close>
  then show ?case
    apply (simp add: mk_ext_field_def)
    apply (rule proj_preserves)
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    done
next
  case (2 x y z) \<comment> \<open> addition is associative \<close>
  then show ?case
    apply (simp add: poly_setI in_carrier_repr_quot)
    apply (simp add: mk_ext_field_def)
    apply (rule equiv_proj' [OF equiv_ext_equiv])
    apply (rule equiv_trans [OF equiv_ext_equiv])
    apply (rule addp_compat)
    apply (rule equiv_sym [OF equiv_ext_equiv repr_of_proj_equiv [OF equiv_ext_equiv]])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    apply (rule equiv_trans [OF equiv_ext_equiv])
    prefer 2
    apply (rule addp_compat)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    apply (rule repr_of_proj_equiv [OF equiv_ext_equiv])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    apply (simp add: padd_assoc in_carrier_repr_quot)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    done
next
  case 3 \<comment> \<open> zero is in carrier \<close>
  show ?case
    apply (simp add: mk_ext_field_def)
    apply (rule proj_preserves)
    apply (simp add: poly_setI)
    done
next
  case (4 x) \<comment> \<open> zero is neutral element \<close>
  then show ?case
    apply (simp add: mk_ext_field_def)
    apply (rule trans [OF _ proj_repr_of_eq [OF equiv_ext_equiv]])
    apply (rule equiv_proj' [OF equiv_ext_equiv])
    apply (rule equiv_trans [OF equiv_ext_equiv])
    apply (rule addp_compat)
    apply (rule equiv_sym [OF equiv_ext_equiv repr_of_proj_equiv [OF equiv_ext_equiv]])
    apply (simp add: poly_setI)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (erule repr_of_quot [OF equiv_ext_equiv])
    apply (simp add: normal_repr_quot)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (erule repr_of_quot [OF equiv_ext_equiv])
    apply assumption
    done
next
  case (5 x) \<comment> \<open> zero is neutral element \<close>
  then show ?case
    apply (simp add: mk_ext_field_def)
    apply (rule trans [OF _ proj_repr_of_eq [OF equiv_ext_equiv]])
    apply (rule equiv_proj' [OF equiv_ext_equiv])
    apply (rule equiv_trans [OF equiv_ext_equiv])
    apply (rule addp_compat)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (erule repr_of_quot [OF equiv_ext_equiv])
    apply (rule equiv_sym [OF equiv_ext_equiv repr_of_proj_equiv [OF equiv_ext_equiv]])
    apply (simp add: poly_setI)
    apply (simp add: normal_repr_quot)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (erule repr_of_quot [OF equiv_ext_equiv])
    apply assumption
    done
next
  case (6 x y) \<comment> \<open> addition is commutative \<close>
  then show ?case
    apply (simp add: mk_ext_field_def)
    apply (rule equiv_proj' [OF equiv_ext_equiv])
    apply (simp add: padd_commut in_carrier_repr_quot)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    done
next
  case 7 \<comment> \<open> additive inverse \<close>
  show ?case
    apply (simp add: mk_ext_field_def Units_def)
    apply (rule subsetI)
    apply simp
    apply (rule_tac x="Equiv_Relations.proj (ext_equiv R p) (-- repr_of x)" in bexI)
    apply (rule conjI)
    apply (rule equiv_proj' [OF equiv_ext_equiv])
    apply (rule equiv_trans [OF equiv_ext_equiv])
    apply (rule addp_compat)
    apply (rule equiv_sym [OF equiv_ext_equiv repr_of_proj_equiv [OF equiv_ext_equiv]])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (erule repr_of_quot [OF equiv_ext_equiv])
    apply (simp add: in_carrier_repr_quot)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (simp add: poly_setI)
    apply (rule equiv_proj' [OF equiv_ext_equiv])
    apply (rule equiv_trans [OF equiv_ext_equiv])
    apply (rule addp_compat)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (erule repr_of_quot [OF equiv_ext_equiv])
    apply (rule equiv_sym [OF equiv_ext_equiv repr_of_proj_equiv [OF equiv_ext_equiv]])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    apply (simp add: in_carrier_repr_quot)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (simp add: poly_setI)
    apply (rule proj_preserves)
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    done
next
  case (8 x y) \<comment> \<open> multiplication is closed \<close>
  then show ?case
    apply (simp add: mk_ext_field_def)
    apply (rule proj_preserves)
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    done
next
  case (9 x y z) \<comment> \<open> multiplication is associative \<close>
  then show ?case
    apply (simp add: mk_ext_field_def)
    apply (rule equiv_proj' [OF equiv_ext_equiv])
    apply (rule equiv_trans [OF equiv_ext_equiv])
    apply (rule pmult_compat)
    apply (rule equiv_sym [OF equiv_ext_equiv repr_of_proj_equiv [OF equiv_ext_equiv]])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (erule repr_of_quot [OF equiv_ext_equiv])
    apply (rule equiv_trans [OF equiv_ext_equiv])
    prefer 2
    apply (rule pmult_compat)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (erule repr_of_quot [OF equiv_ext_equiv])
    apply (rule repr_of_proj_equiv [OF equiv_ext_equiv])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    apply (simp add: pmult_assoc in_carrier_repr_quot)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    done
next
  case 10 \<comment> \<open> one is in carrier \<close>
  show ?case
    apply (simp add: mk_ext_field_def)
    apply (rule proj_preserves)
    apply (simp add: poly_setI pnormal_cons_eq)
    done
next
  case (11 x) \<comment> \<open> one is neutral element \<close>
  then show ?case
    apply (simp add: mk_ext_field_def)
    apply (rule trans [OF _ proj_repr_of_eq [OF equiv_ext_equiv]])
    apply (rule equiv_proj' [OF equiv_ext_equiv])
    apply (rule equiv_trans [OF equiv_ext_equiv])
    apply (rule pmult_compat)
    apply (rule equiv_sym [OF equiv_ext_equiv repr_of_proj_equiv [OF equiv_ext_equiv]])
    apply (simp add: poly_setI pnormal_cons_eq)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (erule repr_of_quot [OF equiv_ext_equiv])
    apply (simp add: in_carrier_repr_quot normal_repr_quot)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (erule repr_of_quot [OF equiv_ext_equiv])
    apply assumption
    done
next
  case (12 x) \<comment> \<open> one is neutral element \<close>
  then show ?case
  apply (simp add: mk_ext_field_def)
  apply (rule trans [OF _ proj_repr_of_eq [OF equiv_ext_equiv]])
  apply (rule equiv_proj' [OF equiv_ext_equiv])
  apply (rule equiv_trans [OF equiv_ext_equiv])
  apply (rule pmult_compat)
  apply (rule equiv_refl [OF equiv_ext_equiv])
  apply (erule repr_of_quot [OF equiv_ext_equiv])
  apply (rule equiv_sym [OF equiv_ext_equiv repr_of_proj_equiv [OF equiv_ext_equiv]])
  apply (simp add: poly_setI pnormal_cons_eq)
  apply (simp add: in_carrier_repr_quot normal_repr_quot)
  apply (rule equiv_refl [OF equiv_ext_equiv])
  apply (erule repr_of_quot [OF equiv_ext_equiv])
  apply assumption
  done
next
  case (13 x y z) \<comment> \<open> distributivity \<close>
  then show ?case
    apply (simp add: mk_ext_field_def)
    apply (rule equiv_proj' [OF equiv_ext_equiv])
    apply (rule equiv_trans [OF equiv_ext_equiv])
    apply (rule pmult_compat)
    apply (rule equiv_sym [OF equiv_ext_equiv repr_of_proj_equiv [OF equiv_ext_equiv]])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (erule repr_of_quot [OF equiv_ext_equiv])
    apply (rule equiv_trans [OF equiv_ext_equiv])
    prefer 2
    apply (rule addp_compat)
    apply (rule equiv_ext_equiv repr_of_proj_equiv [OF equiv_ext_equiv])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    apply (rule equiv_ext_equiv repr_of_proj_equiv [OF equiv_ext_equiv])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    apply (simp add: pmult_l_distr in_carrier_repr_quot)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    done
next
  case (14 x y z) \<comment> \<open> distributivity \<close>
  then show ?case
    apply (simp add: mk_ext_field_def)
    apply (rule equiv_proj' [OF equiv_ext_equiv])
    apply (rule equiv_trans [OF equiv_ext_equiv])
    apply (rule pmult_compat)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (erule repr_of_quot [OF equiv_ext_equiv])
    apply (rule equiv_sym [OF equiv_ext_equiv repr_of_proj_equiv [OF equiv_ext_equiv]])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    apply (rule equiv_trans [OF equiv_ext_equiv])
    prefer 2
    apply (rule addp_compat)
    apply (rule repr_of_proj_equiv [OF equiv_ext_equiv])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    apply (rule repr_of_proj_equiv [OF equiv_ext_equiv])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    apply (simp add: pmult_r_distr in_carrier_repr_quot)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    done
next
  case (15 x y) \<comment> \<open> multiplication is commutative \<close>
  then show ?case
    apply (simp add: mk_ext_field_def)
    apply (rule equiv_proj' [OF equiv_ext_equiv])
    apply (simp add: pmult_comm in_carrier_repr_quot)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (auto dest!: repr_of_quot [OF equiv_ext_equiv])[1]
    done
next
  case 16
  show ?case
    apply (simp add: mk_ext_field_def Units_def)
    apply (rule subset_antisym)
    apply (rule subsetI)
    apply simp
    prefer 2
    apply (rule subsetI)
    apply simp
    apply (rule_tac x="inv_ext_field R p x" in bexI)
    apply (rule conjI)
    apply (rule equiv_proj' [OF equiv_ext_equiv])
    apply (cut_tac x="repr_of x" and
      y="ext_enum_poly R p (Suc (encode_list (Encode R) (repr_of x)))" in pgcd_eq)
    apply (simp add: in_carrier_repr_quot)
    apply (simp add: normal_p carrier_p del: ext_enum_poly.simps)
    apply (simp add: normal_repr_quot)
    apply (simp add: normal_p carrier_p del: ext_enum_poly.simps)
    apply (simp add: inv_ext_field_def del: ext_enum_poly.simps)
    apply (rule equiv_trans [OF equiv_ext_equiv])
    apply (rule pmult_compat)
    apply (rule equiv_sym [OF equiv_ext_equiv repr_of_proj_equiv [OF equiv_ext_equiv]])
    apply (rule poly_setI)
    apply (simp add: normal_p carrier_p normal_repr_quot in_carrier_repr_quot del: ext_enum_poly.simps)
    apply (simp add: normal_p carrier_p normal_repr_quot in_carrier_repr_quot del: ext_enum_poly.simps)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (simp add: repr_of_quot [OF equiv_ext_equiv])
    apply (simp (no_asm) add: ext_equiv_def del: ext_enum_poly.simps)
    apply (cut_tac q="repr_of x"
      and i="Suc (encode_list (Encode R) (repr_of x))" in ext_ideal_coprime [OF carrier_p _ normal_p])
    apply (simp add: in_carrier_repr_quot)
    apply (simp add: normal_repr_quot)
    apply (cut_tac length_p)
    apply auto[1]
    apply (rule notI)
    apply (erule conjE)
    apply (erule notE)
    apply (rule trans [OF sym [OF proj_repr_of_eq [OF equiv_ext_equiv]]])
    apply assumption
    apply (rule equiv_proj' [OF equiv_ext_equiv])
    apply (simp (no_asm) add: ext_equiv_def)
    apply (simp add: normal_repr_quot in_carrier_repr_quot del: ext_enum_poly.simps)
    apply simp
    apply (simp del: ext_enum_poly.simps)
    apply (rule conjI)
    apply (drule sym)
    apply (simp add: normal_repr_quot in_carrier_repr_quot normal_p carrier_p
      pminus_pminus_eq [symmetric] mem_ext_ideal [OF length_p] del: ext_enum_poly.simps)
    apply (rule_tac x="Suc (encode_list (Encode R) (repr_of x))" in exI)
    apply (rule_tac q="-- snd (snd (pgcd (repr_of x)
     (ext_enum_poly R p (Suc (encode_list (Encode R) (repr_of x))))))" in pdividesI)
    apply (simp add: normal_repr_quot in_carrier_repr_quot normal_p carrier_p pmult_comm
      del: ext_enum_poly.simps)
    apply (simp add: normal_repr_quot in_carrier_repr_quot normal_p carrier_p
      del: ext_enum_poly.simps)
    apply (simp add: normal_repr_quot in_carrier_repr_quot normal_p carrier_p pnormal_cons_eq
      del: ext_enum_poly.simps)

    apply (rule equiv_proj' [OF equiv_ext_equiv])
    apply (cut_tac x="repr_of x" and
      y="ext_enum_poly R p (Suc (encode_list (Encode R) (repr_of x)))" in pgcd_eq)
    apply (simp add: in_carrier_repr_quot)
    apply (simp add: normal_p carrier_p del: ext_enum_poly.simps)
    apply (simp add: normal_repr_quot)
    apply (simp add: normal_p carrier_p del: ext_enum_poly.simps)
    apply (simp add: inv_ext_field_def del: ext_enum_poly.simps)
    apply (rule equiv_trans [OF equiv_ext_equiv])
    apply (rule pmult_compat)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (simp add: repr_of_quot [OF equiv_ext_equiv])

    apply (rule equiv_sym [OF equiv_ext_equiv repr_of_proj_equiv [OF equiv_ext_equiv]])
    apply (rule poly_setI)
    apply (simp add: normal_p carrier_p normal_repr_quot in_carrier_repr_quot del: ext_enum_poly.simps)
    apply (simp add: normal_p carrier_p normal_repr_quot in_carrier_repr_quot del: ext_enum_poly.simps)

    apply (simp (no_asm) add: ext_equiv_def del: ext_enum_poly.simps)
    apply (cut_tac q="repr_of x"
      and i="Suc (encode_list (Encode R) (repr_of x))" in ext_ideal_coprime [OF carrier_p _ normal_p])
    apply (simp add: in_carrier_repr_quot)
    apply (simp add: normal_repr_quot)
    apply (cut_tac length_p)
    apply auto[1]
    apply (rule notI)
    apply (erule conjE)
    apply (erule notE)
    apply (rule trans [OF sym [OF proj_repr_of_eq [OF equiv_ext_equiv]]])
    apply assumption
    apply (rule equiv_proj' [OF equiv_ext_equiv])
    apply (simp (no_asm) add: ext_equiv_def)
    apply (simp add: normal_repr_quot in_carrier_repr_quot del: ext_enum_poly.simps)
    apply simp
    apply (simp del: ext_enum_poly.simps)
    apply (rule conjI)
    apply (drule sym)
    apply (simp add: normal_repr_quot in_carrier_repr_quot normal_p carrier_p
      pminus_pminus_eq [symmetric] mem_ext_ideal [OF length_p] del: ext_enum_poly.simps)
    apply (rule_tac x="Suc (encode_list (Encode R) (repr_of x))" in exI)
    apply (rule_tac q="-- snd (snd (pgcd (repr_of x)
      (ext_enum_poly R p (Suc (encode_list (Encode R) (repr_of x))))))" in pdividesI)
    apply (simp add: normal_repr_quot in_carrier_repr_quot normal_p carrier_p pmult_comm
      del: ext_enum_poly.simps)
    apply (simp add: normal_repr_quot in_carrier_repr_quot normal_p carrier_p
      del: ext_enum_poly.simps)
    apply (simp add: normal_repr_quot in_carrier_repr_quot normal_p carrier_p pnormal_cons_eq
      del: ext_enum_poly.simps)

    apply (simp add: inv_ext_field_def del: ext_enum_poly.simps)
    apply (rule proj_preserves)
    apply (rule poly_setI)
    apply (simp add: normal_p carrier_p normal_repr_quot in_carrier_repr_quot del: ext_enum_poly.simps)
    apply (simp add: normal_p carrier_p normal_repr_quot in_carrier_repr_quot del: ext_enum_poly.simps)

    apply (rule notI)
    apply (erule conjE bexE)+
    apply simp
    apply (drule eq_equiv_class' [OF equiv_ext_equiv, rotated 1])
    apply (simp add: poly_set_def pnormal_cons_eq)
    apply (drule equiv_trans [OF equiv_ext_equiv, rotated 1])
    apply (rule pmult_compat)
    apply (rule equiv_refl [OF equiv_ext_equiv])
    apply (simp add: normal_repr_quot in_carrier_repr_quot poly_set_def)
    apply (rule repr_of_proj_equiv [OF equiv_ext_equiv])
    apply (simp add: poly_set_def)
    apply (drule equiv_sym [OF equiv_ext_equiv])
    apply (simp add: ext_equiv_def pnormal_cons_eq mem_ext_ideal [OF length_p carrier_p _ normal_p]
      pdivides_eq carrier_p normal_p)
    apply (erule exE conjE)+
    apply (drule arg_cong [of _ _ length])
    apply (case_tac "q = []")
    apply simp
    apply (cut_tac R=R and i=i in ext_enum_poly_length [OF length_p])
    apply (simp add: pmult_length' normal_p carrier_p
      length_greater_0_conv [symmetric] del: length_greater_0_conv)
    done
next
  case 17 \<comment> \<open> countable \<close>
  show ?case
    apply (simp add: mk_ext_field_def)
    apply (rule_tac x="\<lambda>i. Equiv_Relations.proj (ext_equiv R p) (pnormalize (decode_list (Decode R) i))" in exI)
    apply (rule subset_antisym)
    apply (rule subsetI)
    apply (erule rangeE)
    apply (simp del: decode_list.simps)
    apply (rule proj_preserves)
    apply (rule poly_setI)
    apply (simp del: decode_list.simps)
    apply (simp del: decode_list.simps)
    apply (rule subsetI)
    apply (rule_tac x="encode_list (Encode R) (repr_of x)" in range_eqI)
    apply (simp add: encode_list_inverse [OF encode_inverse_in_carrier [OF in_carrier_repr_quot]]
      normal_repr_quot proj_repr_of_eq [OF equiv_ext_equiv] del: decode_list.simps)
    done
qed

lemma (in ring) poly_closed: "x \<in> carrier R \<Longrightarrow> in_carrier p \<Longrightarrow> (poly p) x \<in> carrier R"
  by (induct p) simp_all

lemma (in ring_hom_cring) poly_map: "x \<in> carrier R \<Longrightarrow> in_carrier\<^bsub>R\<^esub> p \<Longrightarrow>
  (poly\<^bsub>S\<^esub> (map h p)) (h x) = h ((poly\<^bsub>R\<^esub> p) x)"
  by (induct p) (simp_all add: R.poly_closed)

definition cpoly :: "('a, 'm) ring_scheme \<Rightarrow> 'a \<Rightarrow> 'a list" where
  "cpoly R x = pnormalize\<^bsub>R\<^esub> [x]"

definition mk_poly_ring :: "('a, 'm) ring_scheme \<Rightarrow> 'a list ring"
where "mk_poly_ring R =
  \<lparr>carrier=poly_set R,
   monoid.mult=( ***\<^bsub>R\<^esub>),
   one=pnormalize\<^bsub>R\<^esub> [\<one>\<^bsub>R\<^esub>],
   zero=[],
   add=(+++\<^bsub>R\<^esub>)\<rparr>"

locale poly_cring = cring

sublocale poly_cring < P: cring "mk_poly_ring R"
  apply unfold_locales
  apply (simp_all add: mk_poly_ring_def poly_set_def padd_assoc padd_commut padd_commut'
    pmult_assoc pmult_comm pmult_comm' pmult_r_distr del: pnormalize.simps)
  apply (rule subsetI)
  apply (simp add: Units_def)
  apply (rule_tac x="-- x" in exI)
  apply simp
  done

sublocale poly_cring < hom_cp: ring_hom_cring R "mk_poly_ring R" "cpoly R"
  apply unfold_locales
  apply (rule ring_hom_memI)
  apply (simp add: cpoly_def mk_poly_ring_def poly_set_def)
  apply (simp_all add: cpoly_def mk_poly_ring_def del: pnormalize.simps)
  apply (simp_all add: m_comm)
  done

sublocale countable_field_ext < pc: poly_cring
  by unfold_locales

sublocale countable_field_ext <
  hom_eq: ring_hom_cring "mk_poly_ring R" "mk_ext_field R p" "Equiv_Relations.proj (ext_equiv R p)"
  apply unfold_locales
  apply (rule ring_hom_memI)
  apply (simp_all add: mk_poly_ring_def mk_ext_field_def)
  apply (rule proj_preserves)
  apply assumption
  apply (rule equiv_proj' [OF equiv_ext_equiv])
  apply (rule pmult_compat)
  apply (rule repr_of_proj_equiv [OF equiv_ext_equiv])
  apply assumption
  apply (rule repr_of_proj_equiv [OF equiv_ext_equiv])
  apply assumption
  apply (rule equiv_proj' [OF equiv_ext_equiv])
  apply (rule addp_compat)
  apply (rule repr_of_proj_equiv [OF equiv_ext_equiv])
  apply assumption
  apply (rule repr_of_proj_equiv [OF equiv_ext_equiv])
  apply assumption
  done

lemma (in cring) poly_x: "in_carrier p \<Longrightarrow> pnormal p \<Longrightarrow>
  (poly\<^bsub>mk_poly_ring R\<^esub> (map (cpoly R) p)) [\<zero>, \<one>] = p"
  apply (induct p)
  apply (simp add: mk_poly_ring_def)
  apply simp
  apply (simp add: mk_poly_ring_def cpoly_def padd_cCons pnormal_cons_eq)
  done

lemma (in cring) cpoly_in_carrier [simp]: "x \<in> carrier R \<Longrightarrow> in_carrier (cpoly R x)"
  by (simp add: cpoly_def)

lemma (in cring) cpoly_normal [simp]: "pnormal (cpoly R x)"
  by (simp add: cpoly_def)

lemma (in cring) cpoly_map_in_carrier [simp]:
  "in_carrier p \<Longrightarrow> in_carrier\<^bsub>mk_poly_ring R\<^esub> (map (cpoly R) p)"
  by (induct p) (simp_all add: mk_poly_ring_def poly_set_def)

context countable_field_ext
begin

lemma "(poly\<^bsub>mk_ext_field R p\<^esub> (map (Equiv_Relations.proj (ext_equiv R p) o cpoly R) p))
  (Equiv_Relations.proj (ext_equiv R p) [\<zero>\<^bsub>R\<^esub>, \<one>\<^bsub>R\<^esub>]) = \<zero>\<^bsub>mk_ext_field R p\<^esub>"
  using carrier_p normal_p length_p
  apply (simp only: map_comp_map [symmetric])
  apply (simp only: o_def)
  apply (rule trans)
  apply (rule hom_eq.poly_map)
  apply (simp add: mk_poly_ring_def poly_set_def pnormal_cons_eq)
  apply simp
  apply (simp add: poly_x mk_ext_field_def)
  apply (rule equiv_proj' [OF equiv_ext_equiv])
  apply (simp add: ext_equiv_def mem_ext_ideal)
  apply (rule_tac x=0 in exI)
  apply simp
  done

end  

definition mk_canonical_field :: "('a, 'm) ring_scheme \<Rightarrow> nat ring"
where "mk_canonical_field R =
  \<lparr>carrier=Encode R ` carrier R,
   monoid.mult=(\<lambda>a b. Encode R (Decode R a \<otimes>\<^bsub>R\<^esub> Decode R b)),
   one=Encode R \<one>\<^bsub>R\<^esub>,
   zero=Encode R \<zero>\<^bsub>R\<^esub>,
   add=(\<lambda>a b. Encode R (Decode R a \<oplus>\<^bsub>R\<^esub> Decode R b))\<rparr>"

sublocale countable_field < can: countable_field "mk_canonical_field R"
  apply (rule countable_field.intro)
  apply (rule cring.cring_fieldI)
proof (unfold_locales, goal_cases)
  case (1 x y)
  then show ?case
    by (auto simp add: mk_canonical_field_def encode_inverse)
next
  case (2 x y z)
  then show ?case
    by (auto simp add: mk_canonical_field_def encode_inverse a_assoc)
next
  case 3
  then show ?case
    by (simp add: mk_canonical_field_def)
next
  case (4 x)
  then show ?case
    by (auto simp add: mk_canonical_field_def encode_inverse)
next
  case (5 x)
  then show ?case
    by (auto simp add: mk_canonical_field_def encode_inverse)
next
  case (6 x y)
  then show ?case
    by (auto simp add: mk_canonical_field_def encode_inverse a_comm)
next
  case 7
  then show ?case
    apply (auto simp add: mk_canonical_field_def Units_def encode_inverse)
    apply (rule_tac x="\<ominus> xa" in bexI)
    apply (simp_all add: l_neg r_neg)
    done
next
  case (8 x y)
  then show ?case
    by (auto simp add: mk_canonical_field_def encode_inverse)
next
  case (9 x y z)
  then show ?case
    by (auto simp add: mk_canonical_field_def encode_inverse m_assoc)
next
  case 10
  show ?case
    by (simp add: mk_canonical_field_def)
next
  case (11 x)
  then show ?case
    by (auto simp add: mk_canonical_field_def encode_inverse)
next
  case (12 x)
  then show ?case
    by (auto simp add: mk_canonical_field_def encode_inverse)
next
  case (13 x y z)
  then show ?case
    by (auto simp add: mk_canonical_field_def encode_inverse l_distr)
next
  case (14 x y z)
  then show ?case
    by (auto simp add: mk_canonical_field_def encode_inverse r_distr)
next
  case (15 x y)
  then show ?case
    by (auto simp add: mk_canonical_field_def encode_inverse m_comm)
next
  case 16
  then show ?case
    apply (auto simp add: mk_canonical_field_def encode_inverse Units_def)
    apply (drule_tac f="Decode R" in arg_cong)
    apply (simp add: encode_inverse)
    apply (subgoal_tac "xa \<noteq> \<zero>")
    apply (rule_tac x="inv xa" in bexI)
    apply auto
    done
next
  case 17
  show ?case
    by (rule_tac x="\<lambda>n. if n \<in> Encode R ` carrier R then n else encode \<zero>" in exI)
      (auto simp add: mk_canonical_field_def)
qed

end
