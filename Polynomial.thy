theory Polynomial
imports "~~/src/HOL/Algebra/Ring"
begin

definition in_carrier :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> bool" ("in'_carrier\<index> _" [1000] 999) where
  "in_carrier\<^bsub>R\<^esub> xs = (\<forall>x\<in>set xs. x \<in> carrier R)"

lemma in_carrier_Cons_eq [simp]: "in_carrier\<^bsub>R\<^esub> (x # xs) = (x \<in> carrier R \<and> in_carrier\<^bsub>R\<^esub> xs)"
  by (simp add: in_carrier_def)

lemma in_carrier_app_eq [simp]: "in_carrier\<^bsub>R\<^esub> (xs @ ys) = (in_carrier\<^bsub>R\<^esub> xs \<and> in_carrier\<^bsub>R\<^esub> ys)"
  by (auto simp add: in_carrier_def)

lemma in_carrier_Nil [simp]: "in_carrier\<^bsub>R\<^esub> []"
  by (simp add: in_carrier_def)

lemma butlast_closed [simp]: "in_carrier\<^bsub>R\<^esub> xs \<Longrightarrow> in_carrier\<^bsub>R\<^esub> (butlast xs)"
  by (induct xs) simp_all

lemma last_closed [simp]: "in_carrier\<^bsub>R\<^esub> xs \<Longrightarrow> xs \<noteq> [] \<Longrightarrow> last xs \<in> carrier R"
  by (induct xs) simp_all

text \<open>Auxiliary: operations for lists (later) representing coefficients\<close>

definition cCons :: "('a, 'm) ring_scheme \<Rightarrow> 'a \<Rightarrow> 'a list \<Rightarrow> 'a list"  (infixr "##\<index>" 65)
where
  "x ##\<^bsub>R\<^esub> xs = (if xs = [] \<and> x = \<zero>\<^bsub>R\<^esub> then [] else x # xs)"

lemma length_cCons:
  fixes R (structure) shows
  "length (x ## xs) \<le> Suc (length xs)"
  by (simp add: cCons_def)

context cring begin

lemma cCons_0_Nil_eq [simp]:
  "\<zero> ## [] = []"
  by (simp add: cCons_def)

lemma cCons_Cons_eq [simp]:
  "x ## y # ys = x # y # ys"
  by (simp add: cCons_def)

lemma cCons_append_Cons_eq [simp]:
  "x ## xs @ y # ys = x # xs @ y # ys"
  by (simp add: cCons_def)

lemma cCons_not_0_eq [simp]:
  "x \<noteq> \<zero> \<Longrightarrow> x ## xs = x # xs"
  by (simp add: cCons_def)

lemma cCons_cCons_eq [simp]: "(x ## xs = y ## ys) = (x = y \<and> xs = ys)"
  by (auto simp add: cCons_def)

lemma in_carrier_cCons [simp]: "in_carrier (x ## xs) = (x \<in> carrier R \<and> in_carrier xs)"
  by (simp add: cCons_def)

end

primrec pnormalize :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> 'a list" ("pnormalize\<index> _" [1000] 999) where
  pnormalize_Nil: "pnormalize\<^bsub>R\<^esub> [] = []"
| pnormalize_Cons: "pnormalize\<^bsub>R\<^esub> (x # xs) = x ##\<^bsub>R\<^esub> pnormalize\<^bsub>R\<^esub> xs"

definition pnormal :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> bool" ("pnormal\<index> _" [1000] 999) where
  "pnormal\<^bsub>R\<^esub> xs = (xs = pnormalize\<^bsub>R\<^esub> xs)"

lemma pnormalize_length [simp]:
  fixes R (structure) shows
  "length (pnormalize xs) \<le> length xs"
  by (induct xs) (simp_all add: cCons_def)

context cring begin

lemma pnormalize_idemp [simp]: "pnormalize (pnormalize xs) = pnormalize xs"
  by (induct xs) (simp_all add: cCons_def)

lemma pnormalize_idemp' [simp]: "pnormal xs \<Longrightarrow> pnormalize xs = xs"
  by (simp add: pnormal_def)

lemma in_carrier_pnormalize [simp]: "in_carrier (pnormalize xs) = in_carrier xs"
  by (induct xs) simp_all

lemma pnormal_nil [simp]: "pnormal []"
  by (simp add: pnormal_def)

lemma pnormal_cons_eq: "pnormal (x # xs) = (x # xs = x ## xs \<and> pnormal xs)"
  by (simp add: pnormal_def cCons_def)

lemma pnormalize_cCons [simp]: "pnormalize (x ## xs) = x ## pnormalize xs"
  by (simp add: cCons_def)

lemma pnormal_ccons_eq [simp]: "pnormal (x ## xs) = pnormal xs"
  by (simp add: pnormal_def)

lemma pnormal_pnormalize [simp]: "pnormal (pnormalize xs)"
  by (simp add: pnormal_def)

lemma pnormal_last_neq0 [simp]: "xs \<noteq> [] \<Longrightarrow> pnormal xs \<Longrightarrow> last xs \<noteq> \<zero>"
  apply (induct xs)
  apply auto
  apply (simp_all add: pnormal_cons_eq)
  done

end

text \<open>Application of polynomial as a function.\<close>

primrec poly :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> 'a \<Rightarrow> 'a" ("poly\<index> _" [1000] 999)
where
  poly_Nil: "(poly\<^bsub>R\<^esub> []) x = \<zero>\<^bsub>R\<^esub>"
| poly_Cons: "(poly\<^bsub>R\<^esub> (h # t)) x = h \<oplus>\<^bsub>R\<^esub> x \<otimes>\<^bsub>R\<^esub> (poly\<^bsub>R\<^esub> t) x"

subsection \<open>Arithmetic Operations on Polynomials\<close>

text \<open>Addition\<close>

fun padd :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> 'a list \<Rightarrow> 'a list"  (infixl "+++\<index>" 65)
where
  padd_Nil: "[] +++\<^bsub>R\<^esub> ys = pnormalize\<^bsub>R\<^esub> ys"
| padd_Nil2: "xs +++\<^bsub>R\<^esub> [] = pnormalize\<^bsub>R\<^esub> xs"
| padd_Cons: "(x # xs) +++\<^bsub>R\<^esub> (y # ys) = (x \<oplus>\<^bsub>R\<^esub> y) ##\<^bsub>R\<^esub> (xs +++\<^bsub>R\<^esub> ys)"

lemma poly_add_length [simp]:
  fixes R (structure) shows
  "length (p1 +++ p2) \<le> max (length p1) (length p2)"
  by (induct r\<equiv>R p1 p2 rule: padd.induct) (simp_all add: cCons_def)

lemma poly_add_length_less [simp]:
  fixes R (structure) shows
  "length p1 < n \<Longrightarrow> length p2 < n \<Longrightarrow> length (p1 +++ p2) < n"
  by (auto intro: le_less_trans poly_add_length)

lemma poly_add_length_le [simp]:
  fixes R (structure) shows
  "length p1 \<le> n \<Longrightarrow> length p2 \<le> n \<Longrightarrow> length (p1 +++ p2) \<le> n"
  by (auto intro: order_trans poly_add_length)

context cring begin

lemma padd_closed [simp]: "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> in_carrier (xs +++ ys)"
  by (induct r\<equiv>R xs ys rule: padd.induct) (simp_all add: cCons_def)

lemma padd_Nil2' [simp]: "p +++ [] = pnormalize p"
  by (cases p) auto

lemma padd_commut: "in_carrier b \<Longrightarrow> in_carrier a \<Longrightarrow> b +++ a = a +++ b"
  by (induct r\<equiv>R b a rule: padd.induct) (simp_all add: a_comm)

lemma padd_cCons1: "x \<in> carrier R \<Longrightarrow> y \<in> carrier R \<Longrightarrow>
  (x ## xs) +++ (y # ys) = (x \<oplus> y) ## (xs +++ ys)"
  by (auto simp add: cCons_def)

lemma padd_cCons2: "x \<in> carrier R \<Longrightarrow> y \<in> carrier R \<Longrightarrow>
  (x # xs) +++ (y ## ys) = (x \<oplus> y) ## (xs +++ ys)"
  by (auto simp add: cCons_def)

lemma padd_cCons: "x \<in> carrier R \<Longrightarrow> y \<in> carrier R \<Longrightarrow>
  (x ## xs) +++ (y ## ys) = (x \<oplus> y) ## (xs +++ ys)"
  by (auto simp add: cCons_def)

lemma pnormal_padd [simp]: "pnormal (xs +++ ys)"
  by (induct r\<equiv>R xs ys rule: padd.induct) simp_all

lemma pnormalize_padd1 [simp]:
  "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> pnormalize xs +++ ys = xs +++ ys"
  by (induct r\<equiv>R xs ys rule: padd.induct) (simp_all add: padd_cCons1)

lemma pnormalize_padd2 [simp]:
  "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> xs +++ pnormalize ys = xs +++ ys"
  by (induct r\<equiv>R xs ys rule: padd.induct) (simp_all add: padd_cCons2)

lemma padd_assoc: "in_carrier a \<Longrightarrow> in_carrier b \<Longrightarrow> in_carrier c \<Longrightarrow>
  (a +++ b) +++ c = a +++ (b +++ c)"
proof (induct r\<equiv>R a b arbitrary: c rule: padd.induct)
  case (3 x xs y ys c)
  then show ?case by (cases c) (simp_all add: a_assoc padd_cCons1 padd_cCons2)
qed (simp_all del: pnormalize_Cons)

lemma padd_commut': "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> in_carrier zs \<Longrightarrow>
  xs +++ (ys +++ zs) = ys +++ (xs +++ zs)"
  by (simp add: padd_assoc [symmetric] padd_commut [of xs ys])

lemmas padd_ac = padd_assoc padd_commut padd_commut'

lemma padd_length_eq: "pnormal ys \<Longrightarrow> length xs < length ys \<Longrightarrow> length (xs +++ ys) = length ys"
  by (induct r\<equiv>R xs ys rule: padd.induct) (auto simp add: cCons_def pnormal_cons_eq)

end

text \<open>Multiplication by a constant\<close>

primrec cmult :: "('a, 'm) ring_scheme \<Rightarrow> 'a \<Rightarrow> 'a list \<Rightarrow> 'a list"  (infixl "%*\<index>" 70) where
  cmult_Nil: "c %*\<^bsub>R\<^esub> [] = []"
| cmult_Cons: "c %*\<^bsub>R\<^esub> (h # t) = (c \<otimes>\<^bsub>R\<^esub> h) ##\<^bsub>R\<^esub> (c %*\<^bsub>R\<^esub> t)"

lemma poly_cmult_length [simp]:
  fixes R (structure) shows
  "length (a %* p) \<le> length p"
  by (induct p) (auto simp add: cCons_def)

lemma poly_cmult_length_less [simp]:
  fixes R (structure) shows
  "length p < n \<Longrightarrow> length (a %* p) < n"
  by (auto intro: le_less_trans poly_cmult_length)

lemma poly_cmult_length_le [simp]:
  fixes R (structure) shows
  "length p \<le> n \<Longrightarrow> length (a %* p) \<le> n"
  by (auto intro: order_trans poly_cmult_length)

context cring begin

lemma cmult_closed [simp]: "c \<in> carrier R \<Longrightarrow> in_carrier xs \<Longrightarrow> in_carrier (c %* xs)"
  by (induct xs) simp_all

lemma cmult_cCons [simp]: "c \<in> carrier R \<Longrightarrow> c %* (h ## t) = (c \<otimes> h) ## (c %* t)"
  by (auto simp add: cCons_def)

lemma pnormal_cmult [simp]: "pnormal (x %* xs)"
  by (induct xs) simp_all

lemma pnormalize_cmult [simp]: "x \<in> carrier R \<Longrightarrow> x %* pnormalize xs = x %* xs"
  by (induct xs) simp_all

lemma cmult_r_distr: "x \<in> carrier R \<Longrightarrow> in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow>
  x %* (xs +++ ys) = x %* xs +++ x %* ys"
  by (induct r\<equiv>R xs ys rule: padd.induct) (simp_all add: padd_cCons r_distr)

lemma cmult_l_distr: "x \<in> carrier R \<Longrightarrow> y \<in> carrier R \<Longrightarrow> in_carrier xs \<Longrightarrow>
  (x \<oplus> y) %* xs = x %* xs +++ y %* xs"
  by (induct xs) (simp_all add: padd_cCons l_distr)

lemma cmult_zero [simp]: "in_carrier xs \<Longrightarrow> \<zero> %* xs = []"
  by (induct xs) simp_all

lemma cmult_one [simp]: "in_carrier xs \<Longrightarrow> \<one> %* xs = pnormalize xs"
  by (induct xs) simp_all

lemma cmult_cmult [simp]: "a \<in> carrier R \<Longrightarrow> b \<in> carrier R \<Longrightarrow> in_carrier xs \<Longrightarrow>
  a %* (b %* xs) = (a \<otimes> b) %* xs"
  by (induct xs) (simp_all add: m_assoc)

end

text \<open>Multiplication by a polynomial\<close>

primrec pmult :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> 'a list \<Rightarrow> 'a list"  (infixl "***\<index>" 70)
where
  pmult_Nil: "[] ***\<^bsub>R\<^esub> l2 = []"
| pmult_Cons: "(h # t) ***\<^bsub>R\<^esub> l2 = (h %*\<^bsub>R\<^esub> l2) +++\<^bsub>R\<^esub> (\<zero>\<^bsub>R\<^esub> ##\<^bsub>R\<^esub> (t ***\<^bsub>R\<^esub> l2))"

context cring begin

lemma pmult_closed [simp]: "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> in_carrier (xs *** ys)"
  by (induct xs) simp_all

lemma pmult_Nil2 [simp]: "xs *** [] = []"
  by (induct xs) simp_all

lemma pmult_Cons_right [simp]: "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> y \<in> carrier R \<Longrightarrow>
  xs *** (y # ys) = (y %* xs) +++ (\<zero> ## (xs *** ys))"
  by (induct xs) (simp_all add: padd_cCons padd_ac m_comm)

lemma pmult_cCons: "in_carrier l2 \<Longrightarrow> (h ## t) *** l2 = (h %* l2) +++ (\<zero> ## (t *** l2))"
  by (simp add: cCons_def)

lemma pmult_cCons_right: "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> y \<in> carrier R \<Longrightarrow>
  xs *** (y ## ys) = (y %* xs) +++ (\<zero> ## (xs *** ys))"
  by (simp add: cCons_def)

lemma pmult_comm: "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> xs *** ys = ys *** xs"
  by (induct xs) simp_all

lemma cCons0_distr: "\<zero> ## (xs +++ ys) = (\<zero> ## xs) +++ (\<zero> ## ys)"
  by (induct r\<equiv>R xs ys rule: padd.induct) simp_all

lemma pmult_r_distr: "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> in_carrier zs \<Longrightarrow>
  xs *** (ys +++ zs) = xs *** ys +++ xs *** zs"
  by (induct xs) (simp_all add: cmult_r_distr padd_ac cCons0_distr)

lemma pmult_l_distr: "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> in_carrier zs \<Longrightarrow>
  (ys +++ zs) *** xs = ys *** xs +++ zs *** xs"
  by (induct xs) (simp_all add: cmult_r_distr padd_ac cCons0_distr)

lemma pnormalize_pmult1 [simp]: "in_carrier ys \<Longrightarrow> pnormalize xs *** ys = xs *** ys"
  by (induct xs) (simp_all add: pmult_cCons)

lemma pnormalize_pmult2 [simp]: "in_carrier xs \<Longrightarrow> xs *** pnormalize ys = xs *** ys"
  by (induct xs) simp_all

lemma pnormal_pmult [simp]: "pnormal (xs *** ys)"
  by (induct xs) simp_all

lemma cCons0_assoc: "in_carrier ys \<Longrightarrow> (\<zero> ## xs) *** ys = \<zero> ## (xs *** ys)"
  by (simp add: cCons_def)

lemma cmult_left [simp]: "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> c \<in> carrier R \<Longrightarrow>
  (c %* xs) *** ys = c %* (xs *** ys)"
  by (induct xs) (simp_all add: cmult_r_distr pmult_cCons)

lemma cmult_right [simp]: "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> c \<in> carrier R \<Longrightarrow>
  xs *** (c %* ys) = c %* (xs *** ys)"
  by (simp add: pmult_comm [of xs])

lemma pmult_assoc: "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> in_carrier zs \<Longrightarrow>
  xs *** ys *** zs = xs *** (ys *** zs)"
  by (induct xs) (simp_all add: pmult_l_distr cCons0_assoc)

lemma pmult_comm': "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> in_carrier zs \<Longrightarrow>
  xs *** (ys *** zs) = ys *** (xs *** zs)"
  by (simp add: pmult_assoc [symmetric] pmult_comm [of xs ys])

lemma pmult_length: "length (xs *** ys) \<le> length xs + length ys - 1"
  apply (induct xs)
  apply (auto simp add: cCons_def)
  apply (rule poly_add_length_le)
  apply simp
  apply (cases "0 < length ys")
  apply (simp (no_asm_simp))
  apply simp
  done

end

text \<open>subtraction\<close>

definition puminus :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> 'a list" ("--\<index> _" [80] 80) where
  "--\<^bsub>R\<^esub> p = (\<ominus>\<^bsub>R\<^esub> \<one>\<^bsub>R\<^esub>) %*\<^bsub>R\<^esub> p"

definition pminus :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> 'a list \<Rightarrow> 'a list"  (infixl "---\<index>" 65) where
  "xs ---\<^bsub>R\<^esub> ys = xs +++\<^bsub>R\<^esub> --\<^bsub>R\<^esub> ys"

lemma puminus_length [simp]:
  fixes R (structure) shows
  "length (-- p) \<le> length p"
  by (simp add: puminus_def)

lemma puminus_length_less [simp]:
  fixes R (structure) shows
  "length p < n \<Longrightarrow> length (-- p) < n"
  by (auto intro: le_less_trans puminus_length)

lemma puminus_length_le [simp]:
  fixes R (structure) shows
  "length p \<le> n \<Longrightarrow> length (-- p) \<le> n"
  by (auto intro: order_trans puminus_length)

lemma pminus_length_less [simp]:
  fixes R (structure) shows
  "length p1 < n \<Longrightarrow> length p2 < n \<Longrightarrow> length (p1 --- p2) < n"
  by (auto simp add: pminus_def intro: le_less_trans poly_add_length)

lemma pminus_length_le [simp]:
  fixes R (structure) shows
  "length p1 \<le> n \<Longrightarrow> length p2 \<le> n \<Longrightarrow> length (p1 --- p2) \<le> n"
  by (auto simp add: pminus_def intro: order_trans poly_add_length)

context cring begin

lemma puminus_closed [simp]: "in_carrier xs \<Longrightarrow> in_carrier (-- xs)"
  by (simp add: puminus_def)

lemma pnormal_puminus [simp]: "pnormal (-- xs)"
  by (simp add: puminus_def)

lemma pminus_closed [simp]: "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> in_carrier (xs --- ys)"
  by (simp add: pminus_def)

lemma pmult_puminus_left [simp]:
  "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> -- xs *** ys = -- (xs *** ys)"
  by (simp add: puminus_def)

lemma pmult_puminus_right [simp]:
  "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> xs *** -- ys = -- (xs *** ys)"
  by (simp add: puminus_def)

lemma puminus_padd [simp]: "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> -- (xs +++ ys) = -- xs +++ -- ys"
  by (simp add: puminus_def cmult_r_distr)

lemma puminus_puminus [simp]: "in_carrier xs \<Longrightarrow> -- -- xs = pnormalize xs"
  by (simp add: puminus_def l_minus)

lemma left_puminus [simp]: "in_carrier xs \<Longrightarrow> -- xs +++ xs = []"
  using cmult_l_distr [of "\<ominus> \<one>" \<one>, symmetric]
  by (simp add: puminus_def l_neg)

lemma right_puminus [simp]: "in_carrier xs \<Longrightarrow> xs +++ -- xs = []"
  using cmult_l_distr [of \<one> "\<ominus> \<one>", symmetric]
  by (simp add: puminus_def r_neg)

lemma pnormal_pminus [simp]: "pnormal (xs --- ys)"
  by (simp add: pminus_def)

lemma pmult_pminus_r_distr: "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> in_carrier zs \<Longrightarrow>
  xs *** (ys --- zs) = xs *** ys --- xs *** zs"
  by (simp add: pminus_def pmult_r_distr)

lemma pmult_pminus_l_distr: "in_carrier xs \<Longrightarrow> in_carrier ys \<Longrightarrow> in_carrier zs \<Longrightarrow>
  (ys --- zs) *** xs = ys *** xs --- zs *** xs"
  by (simp add: pminus_def pmult_l_distr)

lemma pminus_eq_eq:
  assumes "in_carrier a" "in_carrier b" "in_carrier c" "pnormal a" "pnormal c"
  shows "(a --- b = c) = (a = c +++ b)"
  using assms
  by (auto simp add: pminus_def padd_assoc)

lemma eq_pminus_eq:
  assumes "in_carrier a" "in_carrier b" "in_carrier c" "pnormal a" "pnormal c"
  shows "(a = c --- b) = (a +++ b = c)"
  using assms
  by (auto simp add: pminus_def padd_assoc)

lemma pminus_padd_eq: "in_carrier a \<Longrightarrow> in_carrier b \<Longrightarrow> in_carrier c \<Longrightarrow>
  a --- b +++ c = a +++ c --- b"
  by (simp add: pminus_def padd_ac)

lemma pminus_self [simp]: "in_carrier xs \<Longrightarrow> xs --- xs = []"
  by (simp add: pminus_def)

lemma padd_pminus_eq: "in_carrier a \<Longrightarrow> in_carrier b \<Longrightarrow> in_carrier c \<Longrightarrow>
  a +++ (b --- c) = a +++ b --- c"
  by (simp add: pminus_def padd_assoc)

lemma pminus_cCons2: "x \<in> carrier R \<Longrightarrow> y \<in> carrier R \<Longrightarrow>
  (x # xs) --- (y ## ys) = (x \<ominus> y) ## (xs --- ys)"
  by (simp add: pminus_def puminus_def padd_cCons2 l_minus minus_eq)

lemma puminus_pminus_eq [simp]: "in_carrier a \<Longrightarrow> in_carrier b \<Longrightarrow> -- (a --- b) = b --- a"
  by (simp add: pminus_def padd_commut)

lemma pnormalize_puminus [simp]: "-- pnormalize x = -- x"
  by (simp add: puminus_def)

lemma pnormalize_pminus1 [simp]: "in_carrier a \<Longrightarrow> in_carrier b \<Longrightarrow> pnormalize a --- b = a --- b"
  by (simp add: pminus_def)

lemma pnormalize_pminus2 [simp]: "a --- pnormalize b = a --- b"
  by (simp add: pminus_def)

lemma pminus_pminus_eq:
  "in_carrier a \<Longrightarrow> in_carrier b \<Longrightarrow> in_carrier c \<Longrightarrow> a --- b --- c = a --- (b +++ c)"
  by (simp add: pminus_def padd_assoc)

lemma pminus_padd_cancel [simp]:
  "in_carrier a \<Longrightarrow> in_carrier b \<Longrightarrow> pnormal a \<Longrightarrow> a --- b +++ b = a"
  by (simp add: pminus_def padd_assoc)

lemma puminus_Nil [simp]: "-- [] = []"
  by (simp add: puminus_def)

lemma pminus_Nil [simp]:
  "a --- [] = pnormalize a"
  by (simp add: pminus_def)

lemma pminus_Nil' [simp]:
  "[] --- a = -- a"
  by (simp add: pminus_def)

end

text \<open>Divisibility\<close>

definition pdivides :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> 'a list \<Rightarrow> bool"  (infixl "pdvd\<index>" 70)
  where "p1 pdvd\<^bsub>R\<^esub> p2 \<longleftrightarrow> (\<exists>q. p2 = p1 ***\<^bsub>R\<^esub> q \<and> in_carrier\<^bsub>R\<^esub> q)"

context cring begin

lemma pdivides_eq: "in_carrier p1 \<Longrightarrow> p1 pdvd p2 = (\<exists>q. p2 = p1 *** q \<and> pnormal q \<and> in_carrier q)"
  apply (auto simp add: pdivides_def)
  apply (rule_tac x="pnormalize q" in exI)
  apply simp
  done

lemma pdividesI [intro]: "p2 = p1 *** q \<Longrightarrow> in_carrier q \<Longrightarrow> p1 pdvd p2"
  by (auto simp add: pdivides_def)

lemma pdividesE [elim!]:
  assumes "p1 pdvd p2"
  obtains q where "p2 = p1 *** q" "in_carrier q"
  using assms by (auto simp add: pdivides_def)

lemma cmult_pdvd_cancel:
  assumes "in_carrier p" "a \<in> carrier R" "a %* p pdvd q"
  shows "p pdvd q"
proof -
  from `a %* p pdvd q`
  obtain k where "in_carrier k" "q = a %* p *** k" ..
  with `in_carrier p` `a \<in> carrier R` have "q = p *** (a %* k)" by simp
  moreover from `a \<in> carrier R` `in_carrier k` have "in_carrier (a %* k)" by simp
  ultimately show "p pdvd q" ..
qed

lemma pnormalize_pdvd [simp]: "in_carrier p \<Longrightarrow> pnormalize p pdvd q = p pdvd q"
proof
  assume "p pdvd q" "in_carrier p"
  from `p pdvd q` obtain k where "in_carrier k" "q = p *** k" ..
  with `in_carrier p` have "q = pnormalize p *** k" by simp
  then show "pnormalize p pdvd q" using `in_carrier k` ..
qed auto

lemma pdvd_Nil [simp]: "p pdvd []"
  by (rule pdividesI [of _ _ "[]"]) simp_all

lemma pdvd_refl [simp]: "pnormal p \<Longrightarrow> in_carrier p \<Longrightarrow> p pdvd p"
  by (rule pdividesI [of _ _ "[\<one>]"]) simp_all

lemma pdvd_cmult:
  assumes "in_carrier p" "in_carrier q" "a \<in> carrier R" "p pdvd q"
  shows "p pdvd (a %* q)"
proof -
  from `p pdvd q`
  obtain k where "q = p *** k" "in_carrier k" ..
  with assms have "a %* q = p *** (a %* k)" by simp
  moreover from `in_carrier k` assms have "in_carrier (a %* k)" by simp
  ultimately show "p pdvd (a %* q)" ..
qed

lemma pdvd_trans:
  "in_carrier a \<Longrightarrow> a pdvd b \<Longrightarrow> b pdvd c \<Longrightarrow> a pdvd c"
  by (auto simp add: pmult_assoc)

lemma pdvd_add [simp]:
  assumes "in_carrier a" "a pdvd b" and "a pdvd c"
  shows "a pdvd (b +++ c)"
proof -
  from \<open>a pdvd b\<close> obtain b' where "b = a *** b'" "in_carrier b'" ..
  moreover from \<open>a pdvd c\<close> obtain c' where "c = a *** c'" "in_carrier c'" ..
  ultimately have "b +++ c = a *** (b' +++ c')" using `in_carrier a`
    by (simp add: pmult_r_distr)
  moreover from `in_carrier b'` `in_carrier c'`
  have "in_carrier (b' +++ c')" by simp
  ultimately show ?thesis ..
qed

lemma pdvd_mult [simp]:
  "in_carrier a \<Longrightarrow> in_carrier b \<Longrightarrow>  a pdvd c \<Longrightarrow> a pdvd (b *** c)"
  by (auto simp add: pmult_comm')

lemma pdvd_nil_left_iff: "[] pdvd x = (x = [])"
  by auto

end

context field begin

lemma inv_closed [simp]: "x \<in> carrier R \<Longrightarrow> x \<noteq> \<zero> \<Longrightarrow> inv x \<in> carrier R"
  by (simp add: field_Units)

lemma l_inv [simp]: "x \<in> carrier R \<Longrightarrow> x \<noteq> \<zero> \<Longrightarrow> inv x \<otimes> x = \<one>"
  by (simp add: field_Units)

lemma r_inv [simp]: "x \<in> carrier R \<Longrightarrow> x \<noteq> \<zero> \<Longrightarrow> x \<otimes> inv x = \<one>"
  by (simp add: field_Units)

lemma nonzero_imp_inverse_nonzero:
  assumes "a \<in> carrier R" and "a \<noteq> \<zero>"
  shows "inv a \<noteq> \<zero>"
proof
  assume ianz: "inv a = \<zero>"
  from assms
  have "\<one> = a \<otimes> inv a" by simp
  also with assms have "... = \<zero>" by (simp add: ianz)
  finally have "\<one> = \<zero>" .
  thus False by (simp add: eq_commute)
qed

lemma m_cancel_right1:
  assumes "x \<in> carrier R" "y \<in> carrier R"
  shows "(x = y \<otimes> x) = (x = \<zero> \<or> y = \<one>)"
proof
  assume "x = y \<otimes> x"
  show "x = \<zero> \<or> y = \<one>"
  proof (cases "x = \<zero>")
    case False
    from \<open>x = y \<otimes> x\<close> have "x \<otimes> inv x = y \<otimes> x \<otimes> inv x" by simp
    with False assms show ?thesis
      by (simp add: m_assoc)
  qed simp
next
  assume "x = \<zero> \<or> y = \<one>"
  with assms show "x = y \<otimes> x" by auto
qed

end

definition pdivmod :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> 'a list \<Rightarrow> 'a list \<times> 'a list" ("pdivmod\<index> _ _" [1000, 1000] 999)
where
  "pdivmod\<^bsub>R\<^esub> xs ys =
     (let
        y = inv\<^bsub>R\<^esub> (last ys);
        ys' = butlast ys
      in foldr (\<lambda>r (qs, rs).
        let rs' = r ##\<^bsub>R\<^esub> rs
        in
          if length rs' < length ys
          then (\<zero>\<^bsub>R\<^esub> ##\<^bsub>R\<^esub> qs, rs')
          else
            let q = last rs' \<otimes>\<^bsub>R\<^esub> y
            in (q ##\<^bsub>R\<^esub> qs, butlast rs' ---\<^bsub>R\<^esub> q %*\<^bsub>R\<^esub> ys')) xs ([], []))"

abbreviation pdivide :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> 'a list \<Rightarrow> 'a list"  (infixl "pdiv\<index>" 70)
  where "xs pdiv\<^bsub>R\<^esub> ys \<equiv> fst (pdivmod\<^bsub>R\<^esub> xs ys)"

abbreviation pmodulo :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> 'a list \<Rightarrow> 'a list"  (infixl "pmod\<index>" 70)
  where "xs pmod\<^bsub>R\<^esub> ys \<equiv> snd (pdivmod\<^bsub>R\<^esub> xs ys)"

lemma pdivmod_Nil [simp]:
  fixes R (structure) shows
  "pdivmod [] ys = ([], [])"
  by (simp add: pdivmod_def)

lemma pdivmod_Cons [simp]:
  fixes R (structure) shows
  "pdivmod (x # xs) ys =
     (if length (x ## xs pmod ys) < length ys then
        (\<zero> ## xs pdiv ys,
         x ## xs pmod ys)
      else
        (last (x ## xs pmod ys) \<otimes> inv (last ys) ## xs pdiv ys,
         butlast (x ## xs pmod ys) ---
         (last (x ## xs pmod ys) \<otimes> inv (last ys)) %* butlast ys))"
  by (auto simp add: pdivmod_def Let_def split: prod.split)

lemma pdivmod_length [simp]:
  fixes R (structure) shows
  "ys \<noteq> [] \<Longrightarrow> length (xs pmod ys) < length ys"
proof (induct xs)
  case Nil
  then show ?case by simp
next
  case (Cons x xs)
  then show ?case
    apply simp
    apply (rule impI)
    apply (rule pminus_length_less)
    apply (simp_all add: not_less)
    apply (drule Suc_mono)
    apply (drule le_less_trans [OF length_cCons [of R x]])
    apply (subgoal_tac "length (x ## xs pmod ys) = length ys")
    apply simp_all
    done
qed

context field begin

lemma pdivmod_closed [simp]:
  assumes "in_carrier xs" "in_carrier ys" "ys \<noteq> []" "last ys \<noteq> \<zero>"
  shows "in_carrier (xs pdiv ys)" "in_carrier (xs pmod ys)"
  using assms
  by (induct xs) (simp_all add: length_greater_0_conv [symmetric]
    del: length_greater_0_conv)

lemma pdivmod_eq_aux:
  assumes "in_carrier xs" "in_carrier ys" "length xs = length ys" "ys \<noteq> []" "last ys \<noteq> \<zero>"
  shows "xs --- last xs \<otimes> inv (last ys) %* ys =
    butlast xs --- last xs \<otimes> inv (last ys) %* butlast ys"
  using assms
proof (induct xs arbitrary: ys)
  case Nil
  then show ?case by simp
next
  case (Cons x xs)
  note Cons' = this
  show ?case
  proof (cases ys)
    case Nil
    with Cons show ?thesis by simp
  next
    case (Cons z zs)
    note Cons'' = this
    show ?thesis
    proof (cases zs)
      case Nil
      with Cons' Cons''
      show ?thesis
        by (simp add: m_assoc pminus_cCons2 minus_eq r_neg)
    next
      case (Cons w ws)
      then have "zs \<noteq> []" by simp
      from Cons Cons' Cons'' have "xs \<noteq> []" by auto
      with Cons' Cons'' `zs \<noteq> []`
      show ?thesis by (simp add: pminus_cCons2)
    qed
  qed
qed

lemma pdivmod_eq:
  assumes "in_carrier xs" "in_carrier ys" "ys \<noteq> []" "last ys \<noteq> \<zero>"
  shows "xs pdiv ys *** ys +++ xs pmod ys = pnormalize xs"
  using assms
proof (induct xs)
  case (Cons x xs)
  let ?rs = "x ## xs pmod ys"
  show ?case
  proof (cases "length ?rs < length ys")
    case True
    with Cons show ?thesis
      by (simp add: pmult_cCons padd_cCons)
  next
    case False
    with le_less_trans [OF length_cCons pdivmod_length [OF `ys \<noteq> []`, THEN Suc_mono], of R x R xs]
    have "length (x ## xs pmod ys) = length ys" by (simp add: not_less)
    with Cons have "?rs \<noteq> []" by auto
    moreover with Cons
    have "last ?rs \<otimes> inv last ys %* ys +++
      (\<zero> ## (xs pdiv ys *** ys)) +++ (?rs --- last ?rs \<otimes> inv last ys %* ys) =
      (\<zero> ## (xs pdiv ys *** ys)) +++ ?rs +++
      (last ?rs \<otimes> inv last ys %* ys --- last ?rs \<otimes> inv last ys %* ys)"
      by (simp add: padd_pminus_eq padd_ac del: pminus_self)
    ultimately show ?thesis using Cons False `length (x ## xs pmod ys) = length ys`
      by (simp add: pmult_cCons padd_cCons pdivmod_eq_aux)
  qed
qed simp

lemma pdivmod_eq':
  assumes "in_carrier xs" "in_carrier ys" "ys \<noteq> []" "last ys \<noteq> \<zero>"
  shows "xs pdiv ys *** ys = xs --- xs pmod ys"
proof -
  from assms have "xs pdiv ys *** ys = xs pdiv ys *** ys +++
    (xs pmod ys +++ -- (xs pmod ys))" by simp
  with assms have "xs pdiv ys *** ys = pnormalize xs +++ -- (xs pmod ys)"
    by (simp add: pdivmod_eq [symmetric] padd_assoc)
  with assms show ?thesis by (simp add: pminus_def)
qed

lemma pnormal_pdivmod [simp]:
  shows "pnormal (xs pdiv ys)" "pnormal (xs pmod ys)"
  by (induct xs) simp_all

end

lemma prod_elim:
  assumes H: "x = (y, z)"
  and H': "y = fst x \<Longrightarrow> z = snd x \<Longrightarrow> P"
  shows P
  by (rule H' [unfolded H]) simp_all

lemmas prod_elim' = prod_elim [OF sym]

function
  pgcd :: "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> 'a list \<Rightarrow> 'a list \<times> 'a list \<times> 'a list" ("pgcd\<index> _ _" [1000, 1000] 999)
where
  pgcd_Nil: "y = [] \<Longrightarrow> pgcd\<^bsub>R\<^esub> x y =
     (if x = [] then ([], [], [])
      else (inv\<^bsub>R\<^esub> (last x) %*\<^bsub>R\<^esub> x, [inv\<^bsub>R\<^esub> (last x)], []))"
| pgcd_non_Nil: "y \<noteq> [] \<Longrightarrow> pgcd\<^bsub>R\<^esub> x y =
     (let
        (q, r) = pdivmod\<^bsub>R\<^esub> x y;
        (g, a, b) = pgcd\<^bsub>R\<^esub> y r
      in (g, b, a ---\<^bsub>R\<^esub> b ***\<^bsub>R\<^esub> q))"
  by force+

termination
  by (relation "measure (\<lambda>(R, x, y). length y)") (auto elim: prod_elim')

lemma pgcd_non_Nil' [simp]:
  fixes R (structure) shows
  "y \<noteq> [] \<Longrightarrow> pgcd x y =
     (fst (pgcd y (x pmod y)),
      snd (snd (pgcd y (x pmod y))),
      fst (snd (pgcd y (x pmod y))) ---
      snd (snd (pgcd y (x pmod y))) *** (x pdiv y))"
  by (simp split: prod.split)

declare pgcd_non_Nil [simp del]

context field begin

lemma pgcd_induct [case_names 1 2]:
  assumes Nil: "\<And>y x. y = [] \<Longrightarrow> P x y"
  and Cons: "\<And>y x. y \<noteq> [] \<Longrightarrow> P y (x pmod y) \<Longrightarrow> P x y"
  shows "P x y"
proof (induct r\<equiv>R x y rule: pgcd.induct)
  case (1 y x)
  then show ?case by (rule Nil)
next
  case (2 y x)
  from 2(1) 2(2) [OF surjective_pairing [symmetric] refl]
  show ?case by (rule Cons)
qed

lemma pgcd_closed [simp]:
  assumes "in_carrier x" "in_carrier y" "pnormal x" "pnormal y"
  shows "in_carrier (fst (pgcd x y))"
    "in_carrier (fst (snd (pgcd x y)))" "in_carrier (snd (snd (pgcd x y)))"
  using assms
  by (induct x y rule: pgcd_induct) simp_all

lemma pnormal_pgcd [simp]:
  assumes "in_carrier x" "in_carrier y" "pnormal x" "pnormal y"
  shows "pnormal (fst (pgcd x y))"
    "pnormal (fst (snd (pgcd x y)))" "pnormal (snd (snd (pgcd x y)))"
  using assms
  by (induct x y rule: pgcd_induct)
    (simp_all add: pnormal_cons_eq nonzero_imp_inverse_nonzero)

lemma pgcd_eq:
  assumes "in_carrier x" "in_carrier y" "pnormal x" "pnormal y"
  shows "fst (snd (pgcd x y)) *** x +++ snd (snd (pgcd x y)) *** y = fst (pgcd x y)"
  using assms
proof (induct x y rule: pgcd_induct)
  case (1 y x)
  then show ?case by simp
next
  case (2 y x)
  from 2(1,3-6)
  have "fst (snd (pgcd x y)) *** x +++ snd (snd (pgcd x y)) *** y =
    snd (snd (pgcd y (x pmod y))) *** x +++
    (-- (snd (snd (pgcd y (x pmod y))) *** x)) +++
    (fst (snd (pgcd y (x pmod y))) *** y +++
     snd (snd (pgcd y (x pmod y))) *** (x pmod y))"
    by (simp add: pminus_def pmult_l_distr pmult_r_distr pmult_assoc pdivmod_eq' padd_ac del: right_puminus)
  also from 2 have "\<dots> = fst (pgcd x y)"
    by simp
  finally show ?case .
qed

lemma cmult_pdvd:
  "in_carrier p \<Longrightarrow> a \<in> carrier R \<Longrightarrow> p pdvd q \<Longrightarrow> a \<noteq> \<zero> \<Longrightarrow> a %* p pdvd q"
  by (rule cmult_pdvd_cancel [where a="inv a"]) simp_all

lemma cmult_pdvd_iff:
  "in_carrier p \<Longrightarrow> a \<in> carrier R \<Longrightarrow> a %* p pdvd q \<longleftrightarrow> (if a = \<zero> then q = [] else p pdvd q)"
  by (auto elim: cmult_pdvd cmult_pdvd_cancel)

lemma pdvd_mod_imp_pdvd:
  assumes "k pdvd snd (pdivmod m n)" "k pdvd n"
    "in_carrier k" "in_carrier m" "in_carrier n" "pnormal m" "pnormal n" "n \<noteq> []"
  shows "k pdvd m"
proof -
  from assms have "k pdvd (m pdiv n *** n +++ m pmod n)"
    by simp
  with assms show ?thesis by (simp add: pdivmod_eq)
qed

lemma pgcd_pdvd:
  assumes "in_carrier x" "in_carrier y" "pnormal x" "pnormal y"
  shows "fst (pgcd x y) pdvd x" "fst (pgcd x y) pdvd y"
  using assms
  by (induct x y rule: pgcd_induct)
    (auto simp add: cmult_pdvd_iff nonzero_imp_inverse_nonzero
       dest: pdvd_mod_imp_pdvd del: pdividesE)

lemma poly_cmult_length_eq: "x \<noteq> \<zero> \<Longrightarrow> pnormal xs \<Longrightarrow> x \<in> carrier R \<Longrightarrow> in_carrier xs \<Longrightarrow>
  length (x %* xs) = length xs"
  by (induct xs) (auto simp add: pnormal_cons_eq cCons_def integral_iff)

lemma pmult_length':
  assumes "in_carrier xs" "in_carrier ys" "pnormal xs" "pnormal ys" "xs \<noteq> []" "ys \<noteq> []"
  shows "length (xs *** ys) = length xs + length ys - 1"
  using assms
  apply (induct xs)
  apply simp
  apply (simp add: pnormal_cons_eq pmult_cCons)
  apply (case_tac "xs = []")
  apply simp
  apply (simp add: cCons_def poly_cmult_length_eq split: if_split_asm)
  apply simp
  apply (rule trans)
  apply (rule padd_length_eq)
  apply simp
  apply (simp_all add: cCons_def)
  apply (simp_all add: length_greater_0_conv [symmetric] del: length_greater_0_conv)
  done

lemma poly_idom:
  assumes "in_carrier xs" "in_carrier ys" "pnormal xs" "pnormal ys" "xs \<noteq> []" "ys \<noteq> []"
  shows "xs *** ys \<noteq> []"
  using pmult_length' [OF assms] assms
  by (auto simp add: length_greater_0_conv [symmetric] simp del: length_greater_0_conv)

lemma poly_idom':
  assumes "in_carrier xs" "in_carrier ys" "pnormal xs" "pnormal ys"
  shows "(xs *** ys = []) = (xs = [] \<or> ys = [])"
  using poly_idom [OF assms]
  by auto

lemma pdivmod_unique:
  assumes "ys \<noteq> []"
  and c: "in_carrier q1" "in_carrier q2" "in_carrier r1" "in_carrier r2" "in_carrier ys"
  and n: "pnormal q1" "pnormal q2" "pnormal r1" "pnormal r2" "pnormal ys"
  and "xs = q1 *** ys +++ r1" and "length r1 < length ys"
  and "xs = q2 *** ys +++ r2" and "length r2 < length ys"
  shows "q1 = q2 \<and> r1 = r2"
proof -
  from assms
  have q3: "(q1 --- q2) *** ys = r2 --- r1"
    by (simp add: pmult_pminus_l_distr eq_pminus_eq pminus_eq_eq pminus_padd_eq)
      (simp add: padd_commut)

  show "q1 = q2 \<and> r1 = r2"
  proof (rule ccontr)
    assume H: "\<not> (q1 = q2 \<and> r1 = r2)"
    with q3 c n have "q1 \<noteq> q2"
      by (auto simp add: eq_pminus_eq)
    with c n have "q1 --- q2 \<noteq> []"
      apply -
      apply (rule notI)
      apply (erule notE)
      apply (drule_tac f="\<lambda>z. z +++ q2" in arg_cong)
      apply (simp add: pminus_padd_eq padd_pminus_eq [symmetric])
      done
    then have "0 < length (q1 --- q2)" by simp
    from H q3 c n `ys \<noteq> []` have "r1 \<noteq> r2"
      by (auto simp add: poly_idom' pminus_eq_eq)
    from `length r1 < length ys` `length r2 < length ys`
    have "length (r2 --- r1) < length ys" by simp
    also have "length ys \<le> length (q1 --- q2) - 1 + length ys" by simp
    also have "\<dots> = length ((q1 --- q2) *** ys)"
      using \<open>q1 \<noteq> q2\<close> \<open>ys \<noteq> []\<close> c n `0 < length (q1 --- q2)`
      by (simp add: pmult_length' pminus_eq_eq less_eq_Suc_le)
    also have "\<dots> = length (r2 --- r1)"
      using q3 by simp
    finally have "length (r2 --- r1) < length (r2 --- r1)" .
    then show "False" by simp
  qed
qed

lemma pdivmod_mult_mult:
  assumes "x \<noteq> []" "z \<noteq> []" "in_carrier x" "in_carrier y" "in_carrier z"
    "pnormal x" "pnormal y" "pnormal z"
  shows "x *** y pdiv (x *** z) = y pdiv z"
  and "x *** y pmod (x *** z) = x *** (y pmod z)"
proof -
  have "y pdiv z = x *** y pdiv (x *** z) \<and> x *** (y pmod z) = x *** y pmod (x *** z)"
  proof (cases "y = []")
    case False
    from assms pdivmod_eq [of y z]
    have "y pdiv z *** z +++ y pmod z = y" by simp
    then have "x *** (y pdiv z *** z +++ y pmod z) = x *** y" by simp
    with assms have eq2: "(y pdiv z) *** (x *** z) +++ (x *** (y pmod z)) = x *** y"
      by (simp add: pmult_r_distr pmult_comm pmult_assoc)
    from assms show ?thesis
      apply (rule_tac pdivmod_unique [OF _ _ _ _ _ _ _ _ _ _ _ eq2 [symmetric] _
        pdivmod_eq [of "x *** y" "x *** z", simplified, symmetric]])
      apply (simp_all add: poly_idom')
      apply (cases "y pmod z = []")
      apply (simp_all add: pmult_length')
      apply (insert pdivmod_length [of z R y])
      apply (simp_all add: length_greater_0_conv [symmetric] del: length_greater_0_conv pdivmod_length)
      done
  next
    case True
    then show ?thesis by (simp add: pdivmod_def)
  qed
  then show "x *** y pdiv (x *** z) = y pdiv z"
  and "x *** y pmod (x *** z) = x *** (y pmod z)"
    by simp_all
qed

lemma pdvd_mod: "in_carrier k \<Longrightarrow> pnormal k \<Longrightarrow> n \<noteq> [] \<Longrightarrow>
  k pdvd m \<Longrightarrow> k pdvd n \<Longrightarrow> k pdvd (m pmod n)"
  by (auto simp add: pdivides_eq poly_idom' pdivmod_mult_mult)

lemma pgcd_greatest:
  "k pdvd x \<Longrightarrow> k pdvd y \<Longrightarrow> in_carrier k \<Longrightarrow> pnormal k \<Longrightarrow>
   in_carrier x \<Longrightarrow> pnormal x \<Longrightarrow> in_carrier y \<Longrightarrow> pnormal y \<Longrightarrow> k pdvd fst (pgcd x y)"
  by (induct x y rule: pgcd_induct)
     (simp_all add: pdvd_cmult pdvd_mod)

lemma pdvd_poly_gcd_iff [iff]:
  assumes "in_carrier k" "in_carrier x" "in_carrier y" "pnormal k" "pnormal x" "pnormal y"
  shows "k pdvd fst (pgcd x y) \<longleftrightarrow> k pdvd x \<and> k pdvd y"
  using assms
  by (auto intro!: pgcd_greatest pgcd_pdvd intro: pdvd_trans del: pdividesE)

lemma cmult_integral_iff:
  "pnormal xs \<Longrightarrow> x \<in> carrier R \<Longrightarrow> in_carrier xs \<Longrightarrow> (x %* xs = []) = (x = \<zero> \<or> xs = [])"
  by (induct xs) (auto simp add: cCons_def integral_iff pnormal_cons_eq split: if_split_asm)

lemma cmult_last: "xs \<noteq> [] \<Longrightarrow> x \<noteq> \<zero> \<Longrightarrow> pnormal xs \<Longrightarrow> x \<in> carrier R \<Longrightarrow> in_carrier xs \<Longrightarrow>
  last (x %* xs) = x \<otimes> last xs"
  by (induct xs) (auto simp add: pnormal_cons_eq cCons_def cmult_integral_iff integral_iff)

lemma pgcd_monic: "x \<noteq> [] \<or> y \<noteq> [] \<Longrightarrow> in_carrier x \<Longrightarrow> in_carrier y \<Longrightarrow> pnormal x \<Longrightarrow> pnormal y \<Longrightarrow>
  last (fst (pgcd x y)) = \<one>"
  by (induct x y rule: pgcd_induct) (simp_all add: cmult_last nonzero_imp_inverse_nonzero)

lemma pdvd_imp_degree_le:
  assumes "p pdvd q" "q \<noteq> []" "pnormal p" "in_carrier p"
  shows "length p \<le> length q"
proof (cases "p = []")
  case False
  from \<open>p pdvd q\<close> \<open>in_carrier p\<close> obtain r
  where r: "q = p *** r" "in_carrier r" "pnormal r" by (auto simp add: pdivides_eq)
  with False \<open>q \<noteq> []\<close> have "r \<noteq> []" by auto
  with False r assms show ?thesis
    by (simp add: pmult_length' length_greater_0_conv [symmetric] del: length_greater_0_conv)
qed simp

lemma poly_dvd_antisym:
  assumes coeff: "p \<noteq> [] \<Longrightarrow> q \<noteq> [] \<Longrightarrow> last p = last q"
  assumes dvd1: "p pdvd q" and dvd2: "q pdvd p"
  assumes pq: "in_carrier p" "in_carrier q" "pnormal p" "pnormal q"
  shows "p = q"
proof (cases "p = []")
  case True with dvd1 show "p = q" by auto
next
  case False with dvd2 have "q \<noteq> []" by auto
  have degree: "length p = length q"
    using \<open>p pdvd q\<close> \<open>q pdvd p\<close> \<open>p \<noteq> []\<close> \<open>q \<noteq> []\<close> pq
    by (intro order_antisym pdvd_imp_degree_le)

  from \<open>in_carrier p\<close> \<open>p pdvd q\<close> obtain a
  where a: "q = p *** a" "pnormal a" "in_carrier a" by (auto simp add: pdivides_eq)
  with \<open>q \<noteq> []\<close> have "a \<noteq> []" by auto
  with degree a \<open>p \<noteq> []\<close> \<open>pnormal p\<close> \<open>pnormal a\<close> \<open>in_carrier p\<close> \<open>in_carrier a\<close>
  have "length a = 1"
    by (simp add: pmult_length' length_greater_0_conv [symmetric] del: length_greater_0_conv)
  with coeff a \<open>in_carrier p\<close> \<open>p \<noteq> []\<close> \<open>q \<noteq> []\<close> \<open>pnormal p\<close> show "p = q"
    apply (cases a)
    apply (auto simp add: pnormal_cons_eq cCons_def)
    apply (auto simp add: cmult_last m_cancel_right1 split: if_split_asm)
    done
qed

lemma poly_gcd_unique:
  assumes xyd: "in_carrier x" "in_carrier y" "in_carrier d" "pnormal x" "pnormal y" "pnormal d"
  assumes dvd1: "d pdvd x" and dvd2: "d pdvd y"
    and greatest: "\<And>k. in_carrier k \<Longrightarrow> pnormal k \<Longrightarrow> k pdvd x \<Longrightarrow> k pdvd y \<Longrightarrow> k pdvd d"
    and monic: "x \<noteq> [] \<or> y \<noteq> [] \<Longrightarrow> last d = \<one>"
  shows "fst (pgcd x y) = d"
proof (rule poly_dvd_antisym)
  assume "fst (pgcd x y) \<noteq> []" "d \<noteq> []"
  with xyd show "last (fst (pgcd x y)) = last d"
    by (cases "y = []", cases "x = []", simp, simp_all add: pgcd_monic monic del: pgcd_Nil)
next
  from xyd show "fst (pgcd x y) pdvd d"
    by (intro greatest pgcd_pdvd) simp_all
next
  from dvd1 dvd2 xyd show "d pdvd fst (pgcd x y)"
    by (intro pgcd_greatest) simp_all
qed (simp_all add: xyd)

lemma pgcd_nil_iff:
  assumes "pnormal x" "pnormal y" "in_carrier x" "in_carrier y"
  shows "fst (pgcd x y) = [] \<longleftrightarrow> x = [] \<and> y = []" (is "?P \<longleftrightarrow> ?Q")
proof
  assume ?P then have "[] pdvd fst (pgcd x y)" by simp
  with assms have "[] pdvd x" and "[] pdvd y" by (auto intro: pdvd_trans)
  then show ?Q by (simp add: pdvd_nil_left_iff)
next
  assume ?Q then show ?P by simp
qed

lemma pgcd_assoc: "in_carrier x \<Longrightarrow> in_carrier y \<Longrightarrow> in_carrier z \<Longrightarrow>
  pnormal x \<Longrightarrow> pnormal y \<Longrightarrow> pnormal z \<Longrightarrow>
  fst (pgcd (fst (pgcd x y)) z) = fst (pgcd x (fst (pgcd y z)))"
  by (rule poly_gcd_unique)
    (auto intro: pdvd_trans [of _ "fst (pgcd y z)"] intro!: pgcd_pdvd simp add: pgcd_monic pgcd_nil_iff)

lemma pgcd_commute: "in_carrier x \<Longrightarrow> in_carrier y \<Longrightarrow> pnormal x \<Longrightarrow> pnormal y \<Longrightarrow>
  fst (pgcd x y) = fst (pgcd y x)"
  by (rule poly_gcd_unique) (auto intro: pgcd_pdvd simp add: pgcd_monic)

lemma pgcd_absorb2: "in_carrier x \<Longrightarrow> in_carrier y \<Longrightarrow> pnormal x \<Longrightarrow> pnormal y \<Longrightarrow>
  y \<noteq> [] \<Longrightarrow> y pdvd x \<Longrightarrow> fst (pgcd x y) = inv (last y) %* y"
  apply (rule poly_gcd_unique)
  apply simp_all
  apply (erule pdividesE)
  apply (rule_tac q="last y %* q" in pdividesI)
  apply simp_all
  apply (rule_tac q="[last y]" in pdividesI)
  apply simp_all
  apply (erule pdividesE [of _ y])
  apply (rule_tac q="inv last y %* q" in pdividesI)
  apply (simp_all add: cmult_last nonzero_imp_inverse_nonzero)
  done

end

abbreviation coprime_poly ::
  "('a, 'm) ring_scheme \<Rightarrow> 'a list \<Rightarrow> 'a list \<Rightarrow> bool" ("coprimep\<index> _ _" [1000, 1000] 999)
where "coprimep\<^bsub>R\<^esub> a b \<equiv> (fst (pgcd\<^bsub>R\<^esub> a b) = [\<one>\<^bsub>R\<^esub>])"

lemma (in field) coprime_pdvd:
  "in_carrier p \<Longrightarrow> in_carrier q \<Longrightarrow> in_carrier r \<Longrightarrow>
   pnormal p \<Longrightarrow> pnormal q \<Longrightarrow> pnormal r \<Longrightarrow>
   r pdvd q \<Longrightarrow> coprimep p q \<Longrightarrow> coprimep p r"
  apply (rule poly_gcd_unique)
  apply (simp_all add: pnormal_cons_eq)
  apply (rule pdividesI [of _ _ p])
  apply simp_all
  apply (rule pdividesI [of _ _ r])
  apply simp_all
  apply (drule sym [of _ "[\<one>]"])
  apply simp
  apply (rule pdvd_trans [of _ r])
  apply assumption+
  done

end
